#ifndef LOADER_OBJ_H
#define LOADER_OBJ_H

#include "Loader.h"

namespace nLoaderObj{

	enum flags{
		NORMALS,
		VERTEXES,
		TEXTURE_COORDINATES
	};

};

class CLoaderObj : public CLoader{

private:

	bool mFlags[3];
	vector<GLuint> mNormalsIndexes, mVertexesIndexes, mTextureCoordinatesIndexes;

	void processData();
	void initBoundingBoxMax();
	void initBoundingBoxMin();
	void parseNormals(GLchar ** nTokenBuffer);
	void parseVertexes(GLchar ** nTokenBuffer);
	void parseTextureCoordinates(GLchar ** nTokenBuffer);
	void parseIndexes(GLchar ** nTokenBuffer, const GLuint nTokenBufferSize);
	void updateBoundingBoxMax(const GLfloat x, const GLfloat y, const GLfloat z);
	void updateBoundingBoxMin(const GLfloat x, const GLfloat y, const GLfloat z);

public:

	CLoaderObj();
	~CLoaderObj();
	void load(const GLchar *npFilePath);

};

#endif