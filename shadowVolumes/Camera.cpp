#include "Camera.h"

namespace nCamera{

	GLfloat gMoveSpeed, gRotationSpeed;

};

// <-- Constructor --> //

CCamera::CCamera(){

	mDrag = false;

}

// <-- Destructor --> //

CCamera::~CCamera(){

}

// <-- Lifts camera --> //

void CCamera::lift(const GLfloat nSpeed){

	mPosition += nSpeed * mUpVector;

}

// <-- Walks camera --> //

void CCamera::walk(const GLfloat nSpeed){

	mPosition += nSpeed * mViewVector;

}

// <-- Strafes camera --> //

void CCamera::strafe(const GLfloat nSpeed){

	mPosition += nSpeed * cross(mUpVector, mViewVector);

}

// <-- On mouse move callback --> //

void CCamera::onMouseMove(const GLuint nMousePositionX, const GLuint nMousePositionY){

	if (isDragging()){

		// <-- Calculates new mouse vectors on screen space -> //

		mMouseDelta.x += (nMousePositionX - mMouseOldPosition.x) * nCamera::gRotationSpeed;
		mMouseDelta.y += (nMousePositionY - mMouseOldPosition.y) * nCamera::gRotationSpeed;

		// <-- Constructs rotation matrix, this method avoids gimbal lock using yaw pitch roll angles --> //

		mat4 rotationMatrix = yawPitchRoll(-radians(mMouseDelta.x), radians(mMouseDelta.y), 0.0f);

		// <-- Gets new up, view and right vectors from rotation matrix --> //

		mUpVector = vec3(rotationMatrix * vec4(0, 1, 0, 0));
		mViewVector = vec3(rotationMatrix * vec4(0, 0, 1, 0));
		mRightVector = vec3(rotationMatrix * vec4(1, 0, 0, 0));

	}

	mMouseOldPosition = vec2(nMousePositionX, nMousePositionY);

}

// <-- On keyboard callback --> //

void CCamera::onKeyDown(const nCamera::movementKeys nKey){

	if (nKey == nCamera::VK_W)
		walk(+nCamera::gMoveSpeed);
	else if (nKey == nCamera::VK_S)
		walk(-nCamera::gMoveSpeed);
	else if (nKey == nCamera::VK_A)
		strafe(+nCamera::gMoveSpeed);
	else if (nKey == nCamera::VK_D)
		strafe(-nCamera::gMoveSpeed);
	else if (nKey == nCamera::VK_Q)
		lift(+nCamera::gMoveSpeed);
	else if (nKey == nCamera::VK_E)
		lift(-nCamera::gMoveSpeed);

}

// <-- Gets fov --> //

GLfloat CCamera::getFov(){

	return mFov;

}

// <-- Is dragging --> //

bool CCamera::isDragging(){

	return mDrag;

}

// <-- Gets position --> //

vec3 CCamera::getPosition(){

	return mPosition;

}

// <-- Gets up vector --> //

vec3 CCamera::getUpVector(){

	return mUpVector;

}

// <-- Gets view matrix --> //

mat4 CCamera::getViewMatrix(){

	return lookAt(mPosition, mPosition + mViewVector, mUpVector);

}

// <-- Gets target point --> //

vec3 CCamera::getTargetPoint(){

	return mPosition + mViewVector;

}

// <-- Gets view vector --> //

vec3 CCamera::getViewVector(){

	return mViewVector;

}

// <-- Gets aspect ratio --> //

GLfloat CCamera::getAspectRatio(){

	return mAspectRatio;

}

// <-- Gets projection matrix --> //

mat4 CCamera::getProjectionMatrix(){

	return perspective(mFov, mAspectRatio, mPlanes[nCamera::NEAR_PLANE], mPlanes[nCamera::FAR_PLANE]);

}

// <-- Gets plane distance : near, far, left, right, bottom, up --> //

GLfloat CCamera::getPlaneDistance(const nCamera::frustumPlanes nPlane){

	return mPlanes[nPlane];

}

// <-- Sets fov --> //

void CCamera::setFov(const GLfloat nFov){

	mFov = nFov;

}

// <-- Sets dragging --> //

void CCamera::setDragging(const bool nDrag){

	mDrag = nDrag;

}

// <-- Sets position --> //

void CCamera::setPosition(const vec3 nPosition){

	mPosition = nPosition;

}

// <-- Sets up vector --> //

void CCamera::setUpVector(const vec3 nUpVector){

	mUpVector = nUpVector;

}

// <-- Sets view vector --> //

void CCamera::setViewVector(const vec3 nViewVector){

	mViewVector = nViewVector;

}

// <-- Sets aspect ratio --> //

void CCamera::setAspectRatio(const GLfloat nAspectRatio){

	mAspectRatio = nAspectRatio;

}

// <-- Sets view matrix --> //

void CCamera::setView(const vec3 nPosition, const vec3 nViewVector, const vec3 nUpVector){

	mPosition = nPosition;
	mUpVector = nUpVector;
	mViewVector = nViewVector;

}

// <-- Sets plane distance : near, far, left, right, bottom, up --> //

void CCamera::setPlaneDistance(const nCamera::frustumPlanes nPlane, const GLfloat nDistance){

	mPlanes[nPlane] = nDistance;

}

// <-- Sets projection matrix --> //

void CCamera::setProjection(const GLfloat nFov, const GLfloat nAspectRatio, const GLfloat nNearPlane, const GLfloat nFarPlane){

	mFov = nFov;
	mAspectRatio = nAspectRatio;
	setPlaneDistance(nCamera::FAR_PLANE, nFarPlane);
	setPlaneDistance(nCamera::NEAR_PLANE, nNearPlane);

}