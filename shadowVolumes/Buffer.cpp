#include "Buffer.h"

namespace nBuffer{

	struct bufferProperties mBufferProperties[5] = {
		{ NORMALS_BUFFER, STATIC_DRAW, NORMALS_SIZE, ARRAY_BUFFER, NORMALS_LAYOUT },
		{ VERTEXES_3D_BUFFER, STATIC_DRAW, VERTEXES_3D_SIZE , ARRAY_BUFFER, VERTEXES_3D_LAYOUT },
		{ BOUNDING_BOX_BUFFER, STATIC_DRAW, VERTEXES_3D_SIZE, ARRAY_BUFFER, BOUNDING_BOX_LAYOUT },
		{ VERTEXES_INDEXES_BUFFER, STATIC_DRAW, INDEXES_SIZE, ELEMENT_ARRAY_BUFFER, VERTEXES_INDEXES_LAYOUT },
		{ TEXTURE_COORDINATES_BUFFER, STATIC_DRAW, TEXTURE_COORDINATES_SIZE, ARRAY_BUFFER, TEXTURE_COORDINATES_LAYOUT }
	};

};

// <-- Constructor --> //

CBuffer::CBuffer(){

	mNumberOfBuffers = sizeof(nBuffer::mBufferProperties) / sizeof(nBuffer::bufferProperties);

	mAdjacency = false;
	glGenVertexArrays(1, &mVAO);
	memset(mVBO, 0, mNumberOfBuffers * sizeof(GLuint));
	memset(mVBOSize, 0, mNumberOfBuffers * sizeof(GLuint));

}

// <-- Destructor --> //

CBuffer::~CBuffer(){

	glDeleteBuffers(5, mVBO);
	glDeleteVertexArrays(1, &mVAO);

}

// <-- Draws buffers --> //

void CBuffer::draw(){

	glBindVertexArray(mVAO);
	glDrawElements((mAdjacency) ? GL_TRIANGLES_ADJACENCY : GL_TRIANGLES, mVBOSize[nBuffer::VERTEXES_INDEXES_BUFFER], GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);

}

// <-- Draws bounding box --> //

void CBuffer::drawBoundingBox(){

	glBindVertexArray(mVAO);
	glDrawArrays(GL_LINES, 0, mVBOSize[nBuffer::BOUNDING_BOX_BUFFER]);
	glBindVertexArray(0);

}

// <-- Sets adjacency mode --> //

void CBuffer::setAdjacency(const bool nAdjacency){
	 
	mAdjacency = nAdjacency;

}

// <-- Fills new buffer --> //

void CBuffer::fillBuffer(nBuffer::id nId, const GLuint nSize, void *npData){

	GLuint id = 0, layout = -1, size = 0, usage = 0, target = 0;

	if (!nSize || !npData)
		return;

	for (int k = 0; k < (int)mNumberOfBuffers; ++k)
		if (nId == nBuffer::mBufferProperties[k].mId){

			id     = nBuffer::mBufferProperties[k].mId;
			usage  = nBuffer::mBufferProperties[k].mUse;
			size   = nBuffer::mBufferProperties[k].mSize;
			target = nBuffer::mBufferProperties[k].mTarget;
			layout = nBuffer::mBufferProperties[k].mLayout;

			break;

		}

	glBindVertexArray(mVAO);

		if (id == nBuffer::VERTEXES_INDEXES_BUFFER){

			glGenBuffers(1, &mVBO[id]);
			glBindBuffer(target, mVBO[id]);
			glBufferData(target, nSize * sizeof(GLuint), npData, usage);

		}
		else{

			glGenBuffers(1, &mVBO[id]);
			glEnableVertexAttribArray(layout);
			glBindBuffer(target, mVBO[id]);
			glBufferData(target, nSize * sizeof(GLfloat), npData, usage);
			glVertexAttribPointer(layout, size, GL_FLOAT, GL_FALSE, GL_FALSE, BUFFER_OFFSET(0));

		}

		mVBOSize[id] = nSize;

	glBindVertexArray(0);

}
