#ifndef MAIN_H
#define MAIN_H

#include <glm\glm.hpp>
#include <Core\gl_core_4_3.h>
#include <GLFW\glfw3.h>
#include <AntTweakBar\AntTweakBar.h>

#include "Scene.h"
#include "Camera.h"
#include "GPUProperties.h"
#include "FramebufferColor.h"

using glm::ivec2;

namespace nMain{

	extern CScene *gpScene;
	extern CCamera *gpCamera;
	extern GLuint gFpsCounter;
	extern GLFWwindow *gpWindow;
	extern CGPUProperties *gpGPUProperties;
	extern GLfloat gCurrentTime, gPreviousTime;
	extern CFramebufferColor *gpFramebufferColor;
	extern ivec2 gViewportDimensions, gMousePosition;
	
};

// <-- Inits application --> //

void initApp();

// <-- Clears application --> //

void clearApp();

// <-- Inits GLFW --> //

bool initGLFW();

// <-- Inits OpenGL core --> //

bool initCore();

// <-- Application main loop --> //

void mainLoop();

// <-- Updates application--> //

void updateApp();

// <-- Updates frames per second --> //

void updateFPS();

// <-- Picks a new color from framebuffer --> //

vec3 pickColor(const GLuint nX, const GLuint nY);

// <-- Char callback --> //

void charCallback(GLFWwindow *pWindow, GLuint nChar);

// <-- Mouse move callback --> //

void mouseMoveCallback(GLFWwindow *pWindow, GLdouble nX, GLdouble nY);

// <-- Window resize callback --> //

void windowResizeCallback(GLFWwindow *pWindow, GLint nWidth, GLint nHeight);

// <-- Mouse down callback --> //

void mouseDownCallback(GLFWwindow *pWindow, GLint nButton, GLint nActions, GLint nMods);

// <-- Keyboard callback --> //

void keyboardCallback(GLFWwindow *pWindow, GLint nKey, GLint nScanCode, GLint nActions, GLint nMods);

#endif