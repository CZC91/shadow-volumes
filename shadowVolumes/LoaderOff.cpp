#include "LoaderOff.h"

// <-- Constructor --> //

CLoaderOff::CLoaderOff(){

}

// <-- Destructor --> //

CLoaderOff::~CLoaderOff(){

}

// <-- Inits bounding box max --> //

void CLoaderOff::initBoundingBoxMax(){

	mBoundingBoxMax = vec3(-0xFFFFFF);

}

// <-- Inits bounding box min --> //

void CLoaderOff::initBoundingBoxMin(){

	mBoundingBoxMin = vec3(+0xFFFFFF);

}

// <-- Loads off file --> //

void CLoaderOff::load(const GLchar *nFilePath){

	string line;
	GLchar ** tokenBuffer;
	GLuint tokenBufferSize;

	mFile.open(nFilePath, ios::in);

	getline(mFile, line);

	if (strcmp(line.c_str(), "OFF")){

		printf("%s File could not be opened\n", nFilePath);

	}
	else{

		getline(mFile, line);
		tokenBuffer = getTokens(const_cast<GLchar*>(line.c_str()), " ", tokenBufferSize);

		parseVertexes(atoi(tokenBuffer[0]));
		parseIndexes(atoi(tokenBuffer[1]));

	}

	mFile.close();
	computeBoundingBox();

}

// <-- Parse indexes --> //

void CLoaderOff::parseIndexes(const GLuint nLines){

	string line;
	GLchar ** tokenBuffer;
	GLuint nIndices, index0, index1, index2, tokenBufferSize;

	for (int i = 0; i < (int)nLines; ++i){

		getline(mFile, line);
		tokenBuffer = getTokens(const_cast<GLchar*>(line.c_str()), " ", tokenBufferSize);

		index0 = atoi(tokenBuffer[1]);
		nIndices = atoi(tokenBuffer[0]);

		for (int k = 3; k <= (int)nIndices; ++k){

			index1 = atoi(tokenBuffer[k - 1]);
			index2 = atoi(tokenBuffer[k]);

			mIndexes.push_back(index0);
			mIndexes.push_back(index1);
			mIndexes.push_back(index2);

		}

	}

}

// <-- Parse Vertexes --> //

void CLoaderOff::parseVertexes(const GLuint nLines){

	string line;
	GLchar ** tokenBuffer;
	GLuint tokenBufferSize;

	initBoundingBoxMax();
	initBoundingBoxMin();

	for (int k = 0; k < (int)nLines; ++k){

		getline(mFile, line);
		tokenBuffer = getTokens(const_cast<GLchar*>(line.c_str()), " ", tokenBufferSize);

		mVertexes.push_back(static_cast<GLfloat>(atof(tokenBuffer[0])));
		mVertexes.push_back(static_cast<GLfloat>(atof(tokenBuffer[1])));
		mVertexes.push_back(static_cast<GLfloat>(atof(tokenBuffer[2])));

		updateBoundingBoxMax(mVertexes[static_cast<GLint>(mVertexes.size()) - 3], mVertexes[static_cast<GLint>(mVertexes.size()) - 2], mVertexes[static_cast<GLint>(mVertexes.size()) - 1]);
		updateBoundingBoxMin(mVertexes[static_cast<GLint>(mVertexes.size()) - 3], mVertexes[static_cast<GLint>(mVertexes.size()) - 2], mVertexes[static_cast<GLint>(mVertexes.size()) - 1]);

	}
}

// <-- Updates bounding box max --> //

void CLoaderOff::updateBoundingBoxMax(const GLfloat x, const GLfloat y, const GLfloat z){

	mBoundingBoxMax.x = MAX(mBoundingBoxMax.x, x);
	mBoundingBoxMax.y = MAX(mBoundingBoxMax.y, y);
	mBoundingBoxMax.z = MAX(mBoundingBoxMax.z, z);

}

// <-- Updated bounding box min --> //

void CLoaderOff::updateBoundingBoxMin(const GLfloat x, const GLfloat y, const GLfloat z){

	mBoundingBoxMin.x = MIN(mBoundingBoxMin.x, x);
	mBoundingBoxMin.y = MIN(mBoundingBoxMin.y, y);
	mBoundingBoxMin.z = MIN(mBoundingBoxMin.z, z);

}