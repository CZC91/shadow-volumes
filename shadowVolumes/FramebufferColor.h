#ifndef FRAMEBUFFER_COLOR
#define FRAMEBUFFER_COLOR

#include "Framebuffer.h"

class CFramebufferColor : public CFramebuffer{

private:

public:

	CFramebufferColor();
	~CFramebufferColor();

	void init(const GLuint nWidth, const GLuint nHeight);

};

#endif