#ifndef UI_MESH_H
#define UI_MESH_H

#include "UI.h"

class CUIMesh : public CUI{

private:

	quat mRotation;
	GLfloat mShininess;
	vec3 mKa, mKd, mKs, mScaling, mPosition;

public:

	CUIMesh();
	~CUIMesh();

	void draw();
	void hide();
	void init();
	vec3 getKa();
	vec3 getKd();
	vec3 getKs();
	vec3 getScaling();
	vec3 getPosition();
	quat getRotation();
	GLfloat getShininess();
	void setKa(const vec3 nKa);
	void setKd(const vec3 nKd);
	void setKs(const vec3 nKs);
	void setScaling(const vec3 nScaling);
	void setPosition(const vec3 nPosition);
	void setRotation(const quat nRotation);
	void setShininess(const GLfloat nShininess);

};

#endif