#ifndef LOADER_MESH_H
#define LOADER_MESH_H

#include <vector>
#include "Mesh.h"
#include "LoaderObj.h"
#include "LoaderOff.h"
#include "TextureLoader.h"

using glm::vec3;
using glm::ivec2;
using std::vector;

namespace nLoaderMesh{

	enum types{
		OBJ,
		OFF,
		UNIT_QUAD
	};

};

class CLoaderMesh : public CLoader{

private:

	quat mRotation;
	GLfloat mShininess;
	CLoaderObj *mpLoaderObj;
	CLoaderOff *mpLoaderOff;
	CTextureLoader *mpTextureLoader;
	string mFilePath, mExtension, mAdjacency, mTexturePath;
	vec3 mKa, mKd, mKs, mPosition, mScaling, mTranslation, mPickingColor;

	void parseMeshFile();
	void parseUnitQuad();
	CMesh * createMesh();

public:

	CLoaderMesh();
	~CLoaderMesh();
	vector<CMesh*> load(const GLchar *npFilePath);

};

#endif