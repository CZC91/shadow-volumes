#include "FramebufferShadow.h"

#include <cstdio>

// <-- Constructor --> //

CFramebufferShadow::CFramebufferShadow(){

}

// <-- Destructor --> //

CFramebufferShadow::~CFramebufferShadow(){

	glDeleteTextures(1, &mTexture);
	glDeleteFramebuffers(1, &mFramebuffer);
	glDeleteRenderbuffers(1, &mRenderbuffer);

}

// <-- Inits framebuffer shadow --> //

void CFramebufferShadow::init(const GLuint nWidth, const GLuint nHeight){

	GLuint depthBuffer, ambientBuffer;
	
	mWidth = nWidth;
	mHeight = nHeight;

	// <-- Depth renderbuffer --> //

	glGenRenderbuffers(1, &depthBuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, mWidth, mHeight);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	// <-- Ambient renderbuffer --> //

	glGenRenderbuffers(1, &ambientBuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, ambientBuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA, mWidth, mHeight);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	// <-- Texture 2D --> //

	glGenTextures(1, &mTexture);
	glBindTexture(GL_TEXTURE_2D, mTexture);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, mWidth, mHeight);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glBindTexture(GL_TEXTURE_2D, 0);

	// <-- Framebuffer --> //

	glGenFramebuffers(1, &mFramebuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, mFramebuffer);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, ambientBuffer);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, mTexture, 0);

	GLenum drawBuffers[] = { nFramebuffer::COLOR_ATTACHMENT_0, nFramebuffer::COLOR_ATTACHMENT_1 };
	glDrawBuffers(2, drawBuffers);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

}