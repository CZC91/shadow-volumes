#ifndef UI_LIGHT_H
#define UI_LIGHT_H

#include "UI.h"

namespace nUILight{

	enum LightTypes{
		SPOT,
		POINT,
		DIRECTIONAL
	};

};

class CUILight : public CUI{

private:

	GLuint mType;
	GLfloat mCutoff, mExponent;
	vec3 mLa, mLd, mLs, mPosition, mDirection;
	
public:

	CUILight();
	~CUILight();

	void draw();
	void hide();
	void init();
	vec3 getLa();
	vec3 getLd();
	vec3 getLs();
	GLuint getType();
	vec3 getPosition();
	vec3 getDirection();
	GLfloat getCutoff();
	GLfloat getExponent();
	void setLa(const vec3 nLa);
	void setLd(const vec3 nLd);
	void setLs(const vec3 nLs);
	void setType(const GLuint nType);
	void setCutoff(const GLfloat nCutoff);
	void setPosition(const vec3 nPosition);
	void setDirection(const vec3 nDirection);
	void setExponent(const GLfloat nExponent);

};

#endif