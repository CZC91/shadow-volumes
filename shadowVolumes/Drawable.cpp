#include "Drawable.h"

// <-- Constructor --> //

CDrawable::CDrawable(){

}

// <-- Destructor --> //

CDrawable::~CDrawable(){

}

// <-- Sets adjacency  --> //

void CDrawable::setAdjacency(const bool nAdjacency){

	mpBuffer->setAdjacency(nAdjacency);

}

// <-- Fills a buffer --> //

void CDrawable::fillBuffer(nBuffer::id nId, const GLuint nSize, void *npData){

	mpBuffer->fillBuffer(nId, nSize, npData);

}