#include "UILight.h"

// <-- Constructor --> //

CUILight::CUILight(){

}

// <-- Destructor --> //

CUILight::~CUILight(){

	TwDeleteBar(mpBar);

}

// <-- Draws light user interface --> //

void CUILight::draw(){

	TwDefine("Light visible = true");

	if (mType == nUILight::POINT){

		TwDefine("Light/mDirection visible = false");
		TwDefine("Light/mSeparator0 visible = false");

		TwDefine("Light/mCutoff visible = false");
		TwDefine("Light/mSeparator2 visible = false");

		TwDefine("Light/mExponent visible = false");
		TwDefine("Light/mSeparator3 visible = false");

	}
	else if (mType == nUILight::DIRECTIONAL){

		TwDefine("Light/mCutoff visible = false");
		TwDefine("Light/mSeparator2 visible = false");

		TwDefine("Light/mExponent visible = false");
		TwDefine("Light/mSeparator3 visible = false");

	}

}

// <-- Hides light user interface --> //

void CUILight::hide(){

	TwDefine("Light visible = false");

}

// <-- Inits light user interface --> //

void CUILight::init(){

	// <-- Creating new bar --> //

	mpBar = TwNewBar("Light");

	// <-- Setting parameters to new bar --> //

	TwDefine("Light movable=true");
	TwDefine("Light visible=false");
	TwDefine("Light resizable=false");
	TwDefine("Light color='0 0 100'");
	TwDefine("Light position='20 20'");
	TwDefine("Light fontresizable=false");

	// <-- Creating AntTweakBar direction vector --> //

	TwAddVarRW(mpBar, "mDirection", TW_TYPE_DIR3F, glm::value_ptr(mDirection), "label = 'Direction'");
	TwAddSeparator(mpBar, "mSeparator0", "");

	// <-- Creating three AntTweakBar floats that work as position in 3D space --> //

	TwAddVarRW(mpBar, "mPositionX", TW_TYPE_FLOAT, &mPosition.x, "label='Position X' step = 0.01");
	TwAddVarRW(mpBar, "mPositionY", TW_TYPE_FLOAT, &mPosition.y, "label='Position Y' step = 0.01");
	TwAddVarRW(mpBar, "mPositionZ", TW_TYPE_FLOAT, &mPosition.z, "label='Position Z' step = 0.01");
	TwAddSeparator(mpBar, "mSeparator1", "");

	// <-- Creating AntTweakBar float that works as cutoff --> //

	TwAddVarRW(mpBar, "mCutoff", TW_TYPE_FLOAT, &mCutoff, "label = 'Cutoff' step = 0.01 min = 5 max = 50");
	TwAddSeparator(mpBar, "mSeparator2", "");

	// <-- Creating AntTweakBar float that works as exponent --> //

	TwAddVarRW(mpBar, "mExponent", TW_TYPE_FLOAT, &mExponent, "label = 'Exponent' step = 0.01 min = 5 max = 50");
	TwAddSeparator(mpBar, "mSeparator3", "");

	// <-- Creating three AntTweakBar colors that work as ambiental, diffuse and specular coefficents --> //

	TwAddVarRW(mpBar, "mLa", TW_TYPE_COLOR3F, value_ptr(mLa), "label = 'La'");
	TwAddVarRW(mpBar, "mLd", TW_TYPE_COLOR3F, value_ptr(mLd), "label = 'Ld'");
	TwAddVarRW(mpBar, "mLs", TW_TYPE_COLOR3F, value_ptr(mLs), "label = 'Ls'");
	TwAddSeparator(mpBar, "mSeparator4", "");

}

// <-- Gets ambiental coefficent --> //

vec3 CUILight::getLa(){

	return mLa;

}

// <-- Gets diffuse coefficent --> //

vec3 CUILight::getLd(){

	return mLd;

}

// <-- Gets specular coefficent --> //

vec3 CUILight::getLs(){

	return mLs;

}

// <-- Gets light type --> //

GLuint CUILight::getType(){

	return mType;

}

// <-- Gets position (x,y,z) --> //

vec3 CUILight::getPosition(){

	return mPosition;

}

// <-- Gets direction (x,y,z) --> //

vec3 CUILight::getDirection(){

	return mDirection;

}

// <-- Gets cutoff --> //

GLfloat CUILight::getCutoff(){

	return mCutoff;

}

// <-- Gets exponent --> //

GLfloat CUILight::getExponent(){

	return mExponent;

}

// <-- Sets ambiental coefficent --> //

void CUILight::setLa(const vec3 nLa){

	mLa = nLa;

}

// <-- Sets diffuse coefficent --> //

void CUILight::setLd(const vec3 nLd){

	mLd = nLd;

}

// <-- Sets specular coefficent --> //

void CUILight::setLs(const vec3 nLs){

	mLs = nLs;

}

// <-- Sets light type --> //

void CUILight::setType(const GLuint nType){

	mType = nType;

}

// <-- Sets cutoff --> //

void CUILight::setCutoff(const GLfloat nCutoff){

	mCutoff = nCutoff;

}

// <-- Sets position (x,y,z) --> //

void CUILight::setPosition(const vec3 nPosition){

	mPosition = nPosition;

}

// <-- Sets direction (x,y,z) --> //

void CUILight::setDirection(const vec3 nDirection){

	mDirection = nDirection;

}

// <-- Sets exponent --> //

void CUILight::setExponent(const GLfloat nExponent){

	mExponent = nExponent;

}
