#ifndef UI_H
#define UI_H

#include <glm\glm.hpp>
#include <Core\gl_core_4_3.h>
#include <glm\gtc\type_ptr.hpp>
#include <glm\gtx\quaternion.hpp>
#include <AntTweakBar\AntTweakBar.h>

using glm::vec3;
using glm::quat;
using glm::value_ptr;

class CUI{

protected:

	TwBar *mpBar;

	CUI();
	~CUI();

public:

	virtual void draw() = 0;
	virtual void hide() = 0;
	virtual void init() = 0;

};

#endif