#ifndef MESH_H
#define MESH_H

#include <glm\glm.hpp>
#include <glm\gtx\quaternion.hpp>
#include <glm\gtc\matrix_transform.hpp>

#include "Drawable.h"

using glm::mat4;
using glm::quat;
using glm::vec3;
using glm::scale;
using glm::toMat4;
using glm::translate;

class CMesh : public CDrawable{

private:

	quat mRotation;
	GLuint mTextureID;
	mat4 mModelMatrix;
	GLfloat mShininess;
	bool mShadowCaster;
	vec3 mKa, mKd, mKs, mMidPoint, mScaling, mPosition, mPickingColor;

public:

	CMesh();
	~CMesh();

	void draw();
	void drawBoundingBox();
	bool comparePickingColor(const vec3 nPickingColor);

	vec3 getKa();
	vec3 getKd();
	vec3 getKs();
	vec3 getScaling();
	vec3 getMidPoint();
	vec3 getPosition();
	quat getRotation();
	GLuint getTextureID();
	mat4 getModelMatrix();
	vec3 getPickingColor();
	GLfloat getShininess();
	bool getShadowCaster();

	void setKa(const vec3 nKa);
	void setKd(const vec3 nKd);
	void setKs(const vec3 nKs);
	void setScaling(const vec3 nScaling);
	void setMidPoint(const vec3 nMidPoint);
	void setPosition(const vec3 nPosition);
	void setRotation(const quat nRotation);
	void setTextureID(const GLuint nTextureID);
	void setShininess(const GLfloat nShininess);
	void setPickingColor(const vec3 nPickingColor);
	void setShadowCaster(const bool nShadowCaster);

};

#endif