#ifndef LOADER_LIGHT_H
#define LOADER_LIGHT_H

#include "Light.h"
#include "LoaderOff.h"

using std::vector;

namespace nLoaderLight{

	enum types{
		SPOT,
		POINT,
		DIRECTIONAL
	};

};

class CLoaderLight : public CLoader{

private:

	GLint mType;
	string mFilePath;
	CLoaderOff *mpLoaderOff;
	GLfloat mCutoff, mExponent;
	vec3 mLa, mLd, mLs, mPickingColor, mPosition, mDirection;
	
	void parseLight();
	CLight *createLight();
	
public:

	CLoaderLight();
	~CLoaderLight();
	vector<CLight*> load(const char* npFilePath);

};

#endif