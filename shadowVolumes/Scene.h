#ifndef SCENE_H
#define SCENE_H

#include <vector>
#include <glm\glm.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <glm\gtx\transform.hpp>

#include "Mesh.h"
#include "Light.h"
#include "Camera.h"
#include "UIMesh.h"
#include "UILight.h"
#include "LoaderMesh.h"
#include "LoaderLight.h"
#include "GLSLProgram.h"
#include "FramebufferShadow.h"

using std::vector;
using std::to_string;

using glm::mat3;
using glm::mat4;
using glm::vec3;
using glm::rotate;
using glm::inverse;
using glm::transpose;
using glm::value_ptr;
using glm::infinitePerspective;

namespace nScene{

	enum programs{
		LIGHT,
		PHONG,
		PICKING,
		COMPOSITE,
		BOUNDING_BOX,
		SHADOW_VOLUMES
	};

	enum elements{
		MESH_ELEMENT,
		LIGHT_ELEMENT
	};

	extern GLfloat gAngle;
	extern vec3 gEyePosition;
	extern bool gPickingMode;
	extern mat3 gNormalMatrix;
	extern CFramebufferShadow *gpFramebufferShadow;
	extern mat4 gModelMatrix, gViewMatrix, gModelViewMatrix, gProjectionMatrix, gInfinitePerspectiveProjectionMatrix, gModelViewProjectionMatrix;

};

class CScene{

private:

	bool mShadowMode;
	CUIMesh *mpUIMesh;
	CUILight *mpUILight;
	vector<CMesh*> mMeshes;
	vector<CLight*>mLights;
	vector<CGLSLProgram*> mGLSLProgram;
	GLint mPickedMesh, mPickedLight, mLightToDraw, mMeshToDraw, mActualProgram, mActualElement;
	
	void render();
	void drawLight();
	void drawPhong();
	void drawPass1();
	void drawPass2();
	void drawPass3();
	void drawPicking();
	void drawBoundingBox();
	void drawShadowVolumes();

	void updateMatrices();
	void updatePickedElement();

	void loadMeshInfo();
	void loadLightsInfo();
	void loadMatricesInfo();

	vec3 normalizeColor(const vec3 nColor);

public:

	CScene();
	~CScene();

	void draw();
	void init();
	void drawPick();
	void checkPicking(const vec3 nPickedColor);
};

#endif