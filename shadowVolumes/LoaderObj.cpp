#include "LoaderObj.h"

// <-- Constructor --> //

CLoaderObj::CLoaderObj(){

	for (int k = 0; k < 3; ++k)
		mFlags[k] = false;

}

// <-- Destructor --> //

CLoaderObj::~CLoaderObj(){

}

// <-- Process data --> //

void CLoaderObj::processData(){

	mIndexes = mVertexesIndexes;

	if (mNormalsIndexes.size()){

		vector<GLfloat> tNormals(mVertexes.size(), 0);

		for (int k = 0; k < (int)mNormalsIndexes.size(); ++k){

			tNormals[3 * mVertexesIndexes[k] + 0] = mNormals[3 * mNormalsIndexes[k] + 0];
			tNormals[3 * mVertexesIndexes[k] + 1] = mNormals[3 * mNormalsIndexes[k] + 1];
			tNormals[3 * mVertexesIndexes[k] + 2] = mNormals[3 * mNormalsIndexes[k] + 2];

		}

		mNormals = tNormals;
		tNormals.clear();

	}

	if (mTextureCoordinatesIndexes.size()){

		vector<GLfloat> tTextures(2 * mVertexes.size() / 3, 0);

		for (int k = 0; k < (int)mTextureCoordinatesIndexes.size(); ++k){

			tTextures[2 * mVertexesIndexes[k] + 0] = mTextureCoordinates[2 * mTextureCoordinatesIndexes[k] + 0];
			tTextures[2 * mVertexesIndexes[k] + 1] = mTextureCoordinates[2 * mTextureCoordinatesIndexes[k] + 1];

		}

		mTextureCoordinates = tTextures;
		tTextures.clear();

	}

}

// <-- Inits bounding box max --> //

void CLoaderObj::initBoundingBoxMax(){

	mBoundingBoxMax = vec3(-0xFFFFFF);

}

// <-- Inits bounding box min --> //

void CLoaderObj::initBoundingBoxMin(){

	mBoundingBoxMin = vec3(+0xFFFFFF);

}

// <-- Loads file --> //

void CLoaderObj::load(const GLchar *npFilePath){

	fstream file;
	string fileLine;
	GLchar ** tokenBuffer;
	GLuint tokenBufferSize;

	file.open(npFilePath, ios::in);

	while (getline(file, fileLine)){

		tokenBuffer = getTokens(const_cast<GLchar*>(fileLine.c_str()), " ", tokenBufferSize);

		if (!tokenBuffer) continue;

		if (!strcmp(tokenBuffer[0], "v")){

			parseVertexes(tokenBuffer);

		}
		else if (!strcmp(tokenBuffer[0], "vn")){

			parseNormals(tokenBuffer);

		}
		else if (!strcmp(tokenBuffer[0], "vt")){

			parseTextureCoordinates(tokenBuffer);

		}
		else if (!strcmp(tokenBuffer[0], "f")){

			parseIndexes(tokenBuffer, tokenBufferSize);

		}

	}

	file.close();

	processData();
	computeBoundingBox();

	if (!mFlags[nLoaderObj::NORMALS])
		computeNormals();

}

// <-- Parses normals --> //

void CLoaderObj::parseNormals(GLchar ** nTokenBuffer){

	if (!mFlags[nLoaderObj::NORMALS])
		mFlags[nLoaderObj::NORMALS] = true;

	mNormals.push_back(static_cast<GLfloat>(atof(nTokenBuffer[1])));
	mNormals.push_back(static_cast<GLfloat>(atof(nTokenBuffer[2])));
	mNormals.push_back(static_cast<GLfloat>(atof(nTokenBuffer[3])));

}

// <-- Parses vertexes --> //

void CLoaderObj::parseVertexes(GLchar ** nTokenBuffer){

	if (!mFlags[nLoaderObj::VERTEXES])
		mFlags[nLoaderObj::VERTEXES] = true;

	mVertexes.push_back(static_cast<GLfloat>(atof(nTokenBuffer[1])));
	mVertexes.push_back(static_cast<GLfloat>(atof(nTokenBuffer[2])));
	mVertexes.push_back(static_cast<GLfloat>(atof(nTokenBuffer[3])));

	updateBoundingBoxMax(mVertexes[mVertexes.size() - 3], mVertexes[mVertexes.size() - 2], mVertexes[mVertexes.size() - 1]);
	updateBoundingBoxMin(mVertexes[mVertexes.size() - 3], mVertexes[mVertexes.size() - 2], mVertexes[mVertexes.size() - 1]);


}

// <-- Parses texture coordinates --> //

void CLoaderObj::parseTextureCoordinates(GLchar ** nTokenBuffer){

	if (!mFlags[nLoaderObj::TEXTURE_COORDINATES])
		mFlags[nLoaderObj::TEXTURE_COORDINATES] = true;

	mTextureCoordinates.push_back(static_cast<GLfloat>(atof(nTokenBuffer[1])));
	mTextureCoordinates.push_back(static_cast<GLfloat>(atof(nTokenBuffer[2])));

}

// <-- Parses indexes --> //

void CLoaderObj::parseIndexes(GLchar ** nTokenBuffer, const GLuint nTokenBufferSize){

	GLchar ** tBuffer0, ** tBuffer1, ** tBuffer2;
	GLuint tBuffer0Size, tBuffer1Size, tBuffer2Size;

	tBuffer0 = getTokens(nTokenBuffer[1], "/", tBuffer0Size);

	for (int k = 0; k < (int)nTokenBufferSize - 3; ++k){

		tBuffer1 = getTokens(nTokenBuffer[k + 2], "/", tBuffer1Size);
		tBuffer2 = getTokens(nTokenBuffer[k + 3], "/", tBuffer2Size);

		mVertexesIndexes.push_back(ABS(atoi(tBuffer0[0])) - 1);
		mVertexesIndexes.push_back(ABS(atoi(tBuffer1[0])) - 1);
		mVertexesIndexes.push_back(ABS(atoi(tBuffer2[0])) - 1);

		if (tBuffer0Size > 1 && tBuffer1Size > 1 && tBuffer2Size > 1){

			if (mFlags[nLoaderObj::VERTEXES] && mFlags[nLoaderObj::TEXTURE_COORDINATES] && !mFlags[nLoaderObj::NORMALS]){

				mTextureCoordinatesIndexes.push_back(ABS(atoi(tBuffer0[1])) - 1);
				mTextureCoordinatesIndexes.push_back(ABS(atoi(tBuffer1[1])) - 1);
				mTextureCoordinatesIndexes.push_back(ABS(atoi(tBuffer2[1])) - 1);

			}
			else if (mFlags[nLoaderObj::VERTEXES] && !mFlags[nLoaderObj::TEXTURE_COORDINATES] && mFlags[nLoaderObj::NORMALS]){

				mNormalsIndexes.push_back(ABS(atoi(tBuffer0[1])) - 1);
				mNormalsIndexes.push_back(ABS(atoi(tBuffer1[1])) - 1);
				mNormalsIndexes.push_back(ABS(atoi(tBuffer2[1])) - 1);

			}
			else if (mFlags[nLoaderObj::VERTEXES] && mFlags[nLoaderObj::TEXTURE_COORDINATES] && mFlags[nLoaderObj::NORMALS]){

				mTextureCoordinatesIndexes.push_back(ABS(atoi(tBuffer0[1])) - 1);
				mTextureCoordinatesIndexes.push_back(ABS(atoi(tBuffer1[1])) - 1);
				mTextureCoordinatesIndexes.push_back(ABS(atoi(tBuffer2[1])) - 1);

				mNormalsIndexes.push_back(ABS(atoi(tBuffer0[2])) - 1);
				mNormalsIndexes.push_back(ABS(atoi(tBuffer1[2])) - 1);
				mNormalsIndexes.push_back(ABS(atoi(tBuffer2[2])) - 1);

			}

		}

	}

}

// <-- Updates bounding box max --> //

void CLoaderObj::updateBoundingBoxMax(const GLfloat x, const GLfloat y, const GLfloat z){

	mBoundingBoxMax.x = MAX(mBoundingBoxMax.x, x);
	mBoundingBoxMax.y = MAX(mBoundingBoxMax.y, y);
	mBoundingBoxMax.z = MAX(mBoundingBoxMax.z, z);

}

// <-- Updates bounding box min --> //

void CLoaderObj::updateBoundingBoxMin(const GLfloat x, const GLfloat y, const GLfloat z){

	mBoundingBoxMin.x = MIN(mBoundingBoxMin.x, x);
	mBoundingBoxMin.y = MIN(mBoundingBoxMin.y, y);
	mBoundingBoxMin.z = MIN(mBoundingBoxMin.z, z);

}
