#include "Scene.h"

namespace nScene{

	GLfloat gAngle;
	vec3 gEyePosition;
	bool gPickingMode;
	mat3 gNormalMatrix;
	CFramebufferShadow *gpFramebufferShadow;
	mat4 gModelMatrix, gViewMatrix, gModelViewMatrix, gProjectionMatrix, gInfinitePerspectiveProjectionMatrix, gModelViewProjectionMatrix;

};

// <-- Constructor --> //

CScene::CScene(){

}

// <-- Destructor --> //

CScene::~CScene(){

	for (int k = 0; k < (int)mMeshes.size(); ++k)
		delete mMeshes[k];

	for (int k = 0; k < (int)mLights.size(); ++k)
		delete mLights[k];

	for (int k = 0; k < (int)mGLSLProgram.size(); ++k)
		delete mGLSLProgram[k];

	delete mpUIMesh;
	delete mpUILight;

	mMeshes.clear();
	mLights.clear();
	mGLSLProgram.clear();

}

// <-- Draws current scene using 3 passes --> //

void CScene::draw(){

	drawPass1();
	drawPass2();
	drawPass3();

}

// <-- Inits scene --> //

void CScene::init(){

	try{

		// <-- Creating light program --> //

		printf("\n\n<------------------ LIGHT PROGRAM ------------------>\n\n");

		mGLSLProgram.push_back(new CGLSLProgram());
		mGLSLProgram[nScene::LIGHT]->loadFromFile("Assets/Shaders/light.vert");
		mGLSLProgram[nScene::LIGHT]->loadFromFile("Assets/Shaders/light.frag");
		mGLSLProgram[nScene::LIGHT]->createProgram();
		mGLSLProgram[nScene::LIGHT]->loadUniformVariables();
		mGLSLProgram[nScene::LIGHT]->loadAttributeVariables();

		// <-- Creating phong program --> //

		printf("\n\n<------------------ PHONG PROGRAM ------------------>\n\n");

		mGLSLProgram.push_back(new CGLSLProgram());
		mGLSLProgram[nScene::PHONG]->loadFromFile("Assets/Shaders/phong.vert");
		mGLSLProgram[nScene::PHONG]->loadFromFile("Assets/Shaders/phong.frag");
		mGLSLProgram[nScene::PHONG]->createProgram();
		mGLSLProgram[nScene::PHONG]->loadUniformVariables();
		mGLSLProgram[nScene::PHONG]->loadAttributeVariables();

		printf("\n\n<------------------ PICKING PROGRAM ------------------>\n\n");

		// <-- Creating picking program --> //

		mGLSLProgram.push_back(new CGLSLProgram());
		mGLSLProgram[nScene::PICKING]->loadFromFile("Assets/Shaders/picking.vert");
		mGLSLProgram[nScene::PICKING]->loadFromFile("Assets/Shaders/picking.frag");
		mGLSLProgram[nScene::PICKING]->createProgram();
		mGLSLProgram[nScene::PICKING]->loadUniformVariables();
		mGLSLProgram[nScene::PICKING]->loadAttributeVariables();

		printf("\n\n<------------------ COMPOSITE PROGRAM ------------------>\n\n");

		// <-- Creating composite program --> //

		mGLSLProgram.push_back(new CGLSLProgram());
		mGLSLProgram[nScene::COMPOSITE]->loadFromFile("Assets/Shaders/composite.vert");
		mGLSLProgram[nScene::COMPOSITE]->loadFromFile("Assets/Shaders/composite.frag");
		mGLSLProgram[nScene::COMPOSITE]->createProgram();
		mGLSLProgram[nScene::COMPOSITE]->loadUniformVariables();
		mGLSLProgram[nScene::COMPOSITE]->loadAttributeVariables();

		printf("\n\n<------------------ BOUNDING BOX PROGRAM ------------------>\n\n");

		// <-- Creating bounding box program --> //

		mGLSLProgram.push_back(new CGLSLProgram());
		mGLSLProgram[nScene::BOUNDING_BOX]->loadFromFile("Assets/Shaders/boundingBox.vert");
		mGLSLProgram[nScene::BOUNDING_BOX]->loadFromFile("Assets/Shaders/boundingBox.frag");
		mGLSLProgram[nScene::BOUNDING_BOX]->createProgram();
		mGLSLProgram[nScene::BOUNDING_BOX]->loadUniformVariables();
		mGLSLProgram[nScene::BOUNDING_BOX]->loadAttributeVariables();

		printf("\n\n<------------------ SHADOW VOLUMES PROGRAM ------------------>\n\n");

		// <-- Creating shadow volumes program --> //

		mGLSLProgram.push_back(new CGLSLProgram());
		mGLSLProgram[nScene::SHADOW_VOLUMES]->loadFromFile("Assets/Shaders/shadowVolumes.vert");
		mGLSLProgram[nScene::SHADOW_VOLUMES]->loadFromFile("Assets/Shaders/shadowVolumes.geom");
		mGLSLProgram[nScene::SHADOW_VOLUMES]->loadFromFile("Assets/Shaders/shadowVolumes.frag");
		mGLSLProgram[nScene::SHADOW_VOLUMES]->createProgram();
		mGLSLProgram[nScene::SHADOW_VOLUMES]->loadUniformVariables();
		mGLSLProgram[nScene::SHADOW_VOLUMES]->loadAttributeVariables();

	}
	catch (CAppException &e){

		cerr << e.what() << endl;

	}

	// <-- Inits rotation angle --> //

	nScene::gAngle = 0;

	// <-- Inits shadow mode --> //

	mShadowMode = false;

	// <-- Sets picking mode --> //

	mPickedMesh = -1;
	mPickedLight = -1;
	nScene::gPickingMode = false;

	// <-- Inits mesh user interface --> //

	mpUIMesh = new CUIMesh();
	mpUIMesh->init();

	// <-- Inits lights user interface --> //

	mpUILight = new CUILight();
	mpUILight->init();

	// <-- Loads meshes --> //

	CLoaderMesh *pLoaderMesh = new CLoaderMesh();
	mMeshes = pLoaderMesh->load("Assets/SceneFiles/meshes.in");
	delete pLoaderMesh;

	// <-- Loads lights --> //

	CLoaderLight *pLoaderLight = new CLoaderLight();
	mLights = pLoaderLight->load("Assets/SceneFiles/lights.in");
	delete pLoaderLight;

	glEnable(GL_DEPTH_TEST);
	glClearStencil(0);

}

// <-- Renders scene --> //

void CScene::render(){

	updatePickedElement();

	for (int k = 1; k < static_cast<GLint>(mMeshes.size()); ++k){

		mMeshToDraw = k;
		mActualElement = nScene::MESH_ELEMENT;
		updateMatrices();
		drawPhong();

		if (mPickedMesh == mMeshToDraw)
			drawBoundingBox();
			
	}

	for (int k = 0; k < static_cast<GLint>(mLights.size()); ++k){

		mLightToDraw = k;
		mActualElement = nScene::LIGHT_ELEMENT;
		updateMatrices();
		drawLight();

		if (mPickedLight == mLightToDraw)
			drawBoundingBox();

	}

}

// <-- Draws using picking program --> //

void CScene::drawPick(){

	// <-- Draws meshes --> //

	for (int k = 1; k < static_cast<GLint>(mMeshes.size()); ++k){

		mMeshToDraw = k;
		mActualElement = nScene::MESH_ELEMENT;

		updateMatrices();
		drawPicking();

	}

	// <-- Draws lights --> //

	for (int k = 0; k < static_cast<GLint>(mLights.size()); ++k){

		mLightToDraw = k;
		mActualElement = nScene::LIGHT_ELEMENT;

		updateMatrices();
		drawPicking();

	}

}

// <-- Draws using light program --> //

void CScene::drawLight(){

	vec3 la, ld, ls;

	mActualProgram = nScene::LIGHT;

	mGLSLProgram[nScene::LIGHT]->use();

		// <-- Loads matrices into OpenGL pipeline --> //

		loadMatricesInfo();

		// <-- Loads ambiental coefficent into OpenGL pipeline --> //

		la = mLights[mLightToDraw]->getLa();
		mGLSLProgram[nScene::LIGHT]->loadUniformf("La", nGLSLProgram::UNIFORM_SIZE_3D, value_ptr(la));

		// <-- Loads diffuse coefficent into OpenGL pipeline --> //

		ld = mLights[mLightToDraw]->getLd();
		mGLSLProgram[nScene::LIGHT]->loadUniformf("Ld", nGLSLProgram::UNIFORM_SIZE_3D, value_ptr(ld));

		// <-- Loads specular coefficent into OpenGL pipeline --> //

		ls = mLights[mLightToDraw]->getLs();
		mGLSLProgram[nScene::LIGHT]->loadUniformf("Ls", nGLSLProgram::UNIFORM_SIZE_3D, value_ptr(ls));

		// <-- Draws light using Buffer class --> //

		mLights[mLightToDraw]->draw();

	mGLSLProgram[nScene::LIGHT]->unUse();

}

// <-- Draws using phong program --> //

void CScene::drawPhong(){

	vec3 eyePosition;

	mActualProgram = nScene::PHONG;

	mGLSLProgram[nScene::PHONG]->use();

		// <-- Loads mesh info into OpenGL pipeline --> //

		loadMeshInfo();

		// <-- Loads lights info into OpenGL pipeline --> //

		loadLightsInfo();

		// <-- Loads matrices info into OpenGL pipeline --> //

		loadMatricesInfo();

		// <-- Binds texture into OpenGL pipeline --> //

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, mMeshes[mMeshToDraw]->getTextureID());

		// <-- Loads eye position into OpenGL pipeline --> //

		eyePosition = nScene::gEyePosition;
		mGLSLProgram[nScene::PHONG]->loadUniformf("eyePosition", nGLSLProgram::UNIFORM_SIZE_3D, value_ptr(eyePosition));

		// <-- Draws mesh using Buffer class --> //

		mMeshes[mMeshToDraw]->draw();

	mGLSLProgram[nScene::PHONG]->unUse();

}

// <-- Draws pass 1 for shadow volumes --> //

void CScene::drawPass1(){

	glDepthMask(GL_TRUE);
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_STENCIL_TEST);
	
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	mLights[0]->updatePosition(nScene::gAngle);
	nScene::gProjectionMatrix = nScene::gInfinitePerspectiveProjectionMatrix;
	
	nScene::gpFramebufferShadow->use(nFramebuffer::DEFAULT_TARGET);
		
		glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

		render();

	nScene::gpFramebufferShadow->unUse(nFramebuffer::DEFAULT_TARGET);

}

// <-- Draws pass 2 for shadow volumes --> //

void CScene::drawPass2(){

	mActualProgram = nScene::SHADOW_VOLUMES;

	mGLSLProgram[nScene::SHADOW_VOLUMES]->use();

		// <-- Loads lights position into OpenGL pipeline --> //

		vec3 position = mLights[0]->getPosition();
		mGLSLProgram[nScene::SHADOW_VOLUMES]->loadUniformf("lightPosition", nGLSLProgram::UNIFORM_SIZE_3D, value_ptr(position));

		// <-- Copy depth and color buffers from shadow fbo into default fbo --> //

		glBindFramebuffer(GL_READ_FRAMEBUFFER, nScene::gpFramebufferShadow->getFramebufferId());
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
		glBlitFramebuffer(0, 0, nScene::gpFramebufferShadow->getWidth() - 1, nScene::gpFramebufferShadow->getHeight() - 1, 0, 0, nScene::gpFramebufferShadow->getWidth() - 1, nScene::gpFramebufferShadow->getHeight() - 1, GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT, GL_NEAREST);

		// <-- Disable writing to the color buffer and depth buffer --> //

		glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
		glDepthMask(GL_FALSE);

		// <-- Re-bind to the default framebuffer --> //

		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		// <-- Set up stencil test so that it always suceeds, increments for front faces and decrement for back faces --> //

		glEnable(GL_STENCIL_TEST);
		glClear(GL_STENCIL_BUFFER_BIT);
		glStencilFunc(GL_ALWAYS, 0, 0xffff);
		glStencilOpSeparate(GL_BACK, GL_KEEP, GL_KEEP, GL_DECR_WRAP);
		glStencilOpSeparate(GL_FRONT, GL_KEEP, GL_KEEP, GL_INCR_WRAP);

		glDisable(GL_CULL_FACE);

		// <-- Draws meshes using buffer class --> //

		for (int k = 0; k < static_cast<GLint>(mMeshes.size()); ++k){

			if (mMeshes[k]->getShadowCaster()){

				mMeshToDraw = k;
				mActualElement = nScene::MESH_ELEMENT;
				updateMatrices();

				// <-- Loads matrices into OpenGL pipeline --> //

				loadMatricesInfo();

				// <-- Draws actual mesh using buffer class --> //

				mMeshes[k]->draw();

			}

		}

		// <-- Enable to writing to the color buffer --> //

		glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
		
		glDisable(GL_DEPTH_TEST);

		glStencilFunc(GL_EQUAL, 0, 0xffff);
		glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

	mGLSLProgram[nScene::SHADOW_VOLUMES]->unUse();

}

// <-- Draws pass 3 for shadow volumes --> //

void CScene::drawPass3(){

	nScene::gViewMatrix = mat4(1.0);
	nScene::gModelMatrix = mat4(1.0);
	nScene::gProjectionMatrix = mat4(1.0);

	mActualProgram = nScene::COMPOSITE;

	mGLSLProgram[nScene::COMPOSITE]->use();

		// <-- Loads matrices info into OpenGL pipeline -> //

		loadMatricesInfo();

		// <-- Loads texture unit 0 into OpenGL pipeline --> //

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, nScene::gpFramebufferShadow->getTextureId());

		// <-- Draws mesh using buffer class --> //

		mMeshes[0]->draw();

		glEnable(GL_DEPTH_TEST);

	mGLSLProgram[nScene::COMPOSITE]->unUse();

}

// <-- Draws using picking program --> //

void CScene::drawPicking(){

	vec3 mPickingColor;

	mActualProgram = nScene::PICKING;

	mGLSLProgram[nScene::PICKING]->use();

		// <-- Loads matrices into OpenGL pipeline --> //

		loadMatricesInfo();

		if (mActualElement == nScene::MESH_ELEMENT){

			// <-- Loads mesh picking color into OpenGL pipeline and draws mesh using Buffer class --> //

			mPickingColor = normalizeColor(mMeshes[mMeshToDraw]->getPickingColor());
			mGLSLProgram[nScene::PICKING]->loadUniformf("pickingColor", nGLSLProgram::UNIFORM_SIZE_3D, value_ptr(mPickingColor));
			mMeshes[mMeshToDraw]->draw();

		}
		else{

			// <-- Loads light picking color into OpenGL pipeline and draws light using Buffer class --> //

			mPickingColor = normalizeColor(mLights[mLightToDraw]->getPickingColor());
			mGLSLProgram[nScene::PICKING]->loadUniformf("pickingColor", nGLSLProgram::UNIFORM_SIZE_3D, value_ptr(mPickingColor));
			mLights[mLightToDraw]->draw();

		}

	mGLSLProgram[nScene::PICKING]->unUse();

}

// <-- Draws using bounding box program --> //

void CScene::drawBoundingBox(){

	mActualProgram = nScene::BOUNDING_BOX;

	mGLSLProgram[nScene::BOUNDING_BOX]->use();

		// <-- Loads matrices into OpenGL pipeline --> //

		loadMatricesInfo();

		// <-- Draws bounding box using Buffer class --> //

		(mActualElement == nScene::MESH_ELEMENT) ? mMeshes[mMeshToDraw]->drawBoundingBox() : mLights[mLightToDraw]->drawBoundingBox();

	mGLSLProgram[nScene::BOUNDING_BOX]->unUse();

}

// <-- Updates matrices of current scene --> //

void CScene::updateMatrices(){

	// <-- Gets model matrix from actual mesh or actual light --> //

	if (mActualElement == nScene::MESH_ELEMENT){

		nScene::gModelMatrix = mMeshes[mMeshToDraw]->getModelMatrix();

	}
	else{

		nScene::gModelMatrix = mLights[mLightToDraw]->getModelMatrix();

	}

	// <-- Compute normal matrix based on model matrix --> //

	nScene::gNormalMatrix = inverse(transpose(mat3(vec3(nScene::gModelMatrix[0]), vec3(nScene::gModelMatrix[1]), vec3(nScene::gModelMatrix[2]))));

	// <-- Compute model view matrix --> //

	nScene::gModelViewMatrix = nScene::gViewMatrix * nScene::gModelMatrix;

	// <-- Compute model view projection matrix --> //

	nScene::gModelViewProjectionMatrix = nScene::gProjectionMatrix * nScene::gViewMatrix * nScene::gModelMatrix;

}

// <-- Updates picked element attributes --> //

void CScene::updatePickedElement(){

	if (mPickedLight > -1){

		// <-- Updates picked light --> //

		mLights[mPickedLight]->setLa(mpUILight->getLa());
		mLights[mPickedLight]->setLd(mpUILight->getLd());
		mLights[mPickedLight]->setLs(mpUILight->getLs());
		mLights[mPickedLight]->setType(mpUILight->getType());
		mLights[mPickedLight]->setCutoff(mpUILight->getCutoff());
		mLights[mPickedLight]->setExponent(mpUILight->getExponent());
		mLights[mPickedLight]->setPosition(mpUILight->getPosition());
		mLights[mPickedLight]->setDirection(mpUILight->getDirection());


	}
	else if (mPickedMesh > -1){

		// <-- Updates picked mesh --> //

		mMeshes[mPickedMesh]->setKa(mpUIMesh->getKa());
		mMeshes[mPickedMesh]->setKd(mpUIMesh->getKd());
		mMeshes[mPickedMesh]->setKs(mpUIMesh->getKs());
		mMeshes[mPickedMesh]->setScaling(mpUIMesh->getScaling());
		mMeshes[mPickedMesh]->setPosition(mpUIMesh->getPosition());
		mMeshes[mPickedMesh]->setRotation(mpUIMesh->getRotation());
		mMeshes[mPickedMesh]->setShininess(mpUIMesh->getShininess());

	}

}

// <-- Loads mesh properties into OpenGL pipeline -> //

void CScene::loadMeshInfo(){

	vec3 ka, kd, ks;
	GLfloat shininess;

	// <-- Loads shininess into OpenGL pipeline --> //

	shininess = mMeshes[mMeshToDraw]->getShininess();
	mGLSLProgram[mActualProgram]->loadUniformf("mesh.mShininess", nGLSLProgram::UNIFORM_SIZE_1D, &shininess);

	// <-- Loads ambiental coefficent into OpenGL pipeline --> //

	ka = mMeshes[mMeshToDraw]->getKa();
	mGLSLProgram[mActualProgram]->loadUniformf("mesh.mKa", nGLSLProgram::UNIFORM_SIZE_3D, value_ptr(ka));

	// <-- Loads diffuse coefficent into OpenGL pipeline --> //

	kd = mMeshes[mMeshToDraw]->getKd();
	mGLSLProgram[mActualProgram]->loadUniformf("mesh.mKd", nGLSLProgram::UNIFORM_SIZE_3D, value_ptr(kd));

	// <-- Loads specular coefficent into OpenGL pipeline --> //

	ks = mMeshes[mMeshToDraw]->getKs();
	mGLSLProgram[mActualProgram]->loadUniformf("mesh.mKs", nGLSLProgram::UNIFORM_SIZE_3D, value_ptr(ks));

}

// <-- Load lights info into OpenGL pipeline --> //

void CScene::loadLightsInfo(){

	GLint type;
	GLfloat cutoff, exponent;
	vec3 la, ld, ls, position, direction;

	for (int k = 0; k < static_cast<GLint>(mLights.size()); ++k){

		// <-- Loads type into OpenGL pipeline --> //

		type = mLights[k]->getType();
		mGLSLProgram[mActualProgram]->loadUniformi("lights[" + to_string(k) + "].mType", nGLSLProgram::UNIFORM_SIZE_1D, &type);

		// <-- Loads cutoff into OpenGL pipeline --> //

		cutoff = mLights[k]->getCutoff();
		mGLSLProgram[mActualProgram]->loadUniformf("lights[" + to_string(k) + "].mCutoff", nGLSLProgram::UNIFORM_SIZE_1D, &cutoff);

		// <-- Loads ambiental coefficent into OpenGL pipeline --> //

		la = mLights[k]->getLa();
		mGLSLProgram[mActualProgram]->loadUniformf("lights[" + to_string(k) + "].mLa", nGLSLProgram::UNIFORM_SIZE_3D, value_ptr(la));

		// <-- Loads diffuse coefficent into OpenGL pipeline --> //

		ld = mLights[k]->getLd();
		mGLSLProgram[mActualProgram]->loadUniformf("lights[" + to_string(k) + "].mLd", nGLSLProgram::UNIFORM_SIZE_3D, value_ptr(ld));

		// <-- Loads specular coefficent into OpenGL pipeline --> //

		ls = mLights[k]->getLs();
		mGLSLProgram[mActualProgram]->loadUniformf("lights[" + to_string(k) + "].mLs", nGLSLProgram::UNIFORM_SIZE_3D, value_ptr(ls));

		// <-- Loads exponent into OpenGL pipeline --> //

		exponent = mLights[k]->getExponent();
		mGLSLProgram[mActualProgram]->loadUniformf("lights[" + to_string(k) + "].mExponent", nGLSLProgram::UNIFORM_SIZE_1D, &exponent);

		// <-- Loads position into OpenGL pipeline --> //

		position = mLights[k]->getPosition();
		mGLSLProgram[mActualProgram]->loadUniformf("lights[" + to_string(k) + "].mPosition", nGLSLProgram::UNIFORM_SIZE_3D, value_ptr(position));

		// <-- Loads direction into OpenGL pipeline --> //

		direction = mLights[k]->getDirection();
		mGLSLProgram[mActualProgram]->loadUniformf("lights[" + to_string(k) + "].mDirection", nGLSLProgram::UNIFORM_SIZE_3D, value_ptr(direction));

	}

}

// <-- Load matrices info into OpenGL pipeline --> //

void CScene::loadMatricesInfo(){

	mat3 normalMatrix;
	mat4 modelMatrix, viewMatrix, projectionMatrix, modelViewMatrix, modelViewProjectionMatrix;

	// <-- Loads view matrix into OpenGL pipeline --> //

	viewMatrix = nScene::gViewMatrix;
	mGLSLProgram[mActualProgram]->loadUniformMatrix("viewMatrix", nGLSLProgram::UNIFORM_SIZE_4X4, value_ptr(viewMatrix));

	// <-- Loads model matrix into OpenGL pipeline --> //

	modelMatrix = nScene::gModelMatrix;
	mGLSLProgram[mActualProgram]->loadUniformMatrix("modelMatrix", nGLSLProgram::UNIFORM_SIZE_4X4, value_ptr(modelMatrix));

	// <-- Loads normal matrix into OpenGL pipeline --> //

	normalMatrix = nScene::gNormalMatrix;
	mGLSLProgram[mActualProgram]->loadUniformMatrix("normalMatrix", nGLSLProgram::UNIFORM_SIZE_3X3, value_ptr(normalMatrix));

	// <-- Loads model view matrix into OpenGL pipeline --> //

	modelViewMatrix = nScene::gModelViewMatrix;
	mGLSLProgram[mActualProgram]->loadUniformMatrix("modelViewMatrix", nGLSLProgram::UNIFORM_SIZE_4X4, value_ptr(modelViewMatrix));

	// <-- Loads projection matrix into OpenGL pipeline --> //

	projectionMatrix = nScene::gProjectionMatrix;
	mGLSLProgram[mActualProgram]->loadUniformMatrix("projectionMatrix", nGLSLProgram::UNIFORM_SIZE_4X4, value_ptr(projectionMatrix));

	// <-- Loads model view projection matrix into OpenGL pipeline --> //

	modelViewProjectionMatrix = nScene::gModelViewProjectionMatrix;
	mGLSLProgram[mActualProgram]->loadUniformMatrix("modelViewProjectionMatrix", nGLSLProgram::UNIFORM_SIZE_4X4, value_ptr(modelViewProjectionMatrix));

}

// <-- Normalizes a color --> //

vec3 CScene::normalizeColor(const vec3 nColor){

	return vec3(nColor.x / 0xff, nColor.y / 0xff, nColor.z / 0xff);

}

// <-- Checks piking color of each element of current scene --> //

void CScene::checkPicking(const vec3 nPickedColor){

	mPickedLight = mPickedMesh = -1;

	// <-- Test if we picked a mesh --> //

	for (int k = 0; k < static_cast<GLint>(mMeshes.size()); ++k)
		if (mMeshes[k]->comparePickingColor(nPickedColor)){

			mPickedMesh = k;

			mpUIMesh->setKa(mMeshes[k]->getKa());
			mpUIMesh->setKd(mMeshes[k]->getKd());
			mpUIMesh->setKs(mMeshes[k]->getKs());
			mpUIMesh->setScaling(mMeshes[k]->getScaling());
			mpUIMesh->setPosition(mMeshes[k]->getPosition());
			mpUIMesh->setRotation(mMeshes[k]->getRotation());
			mpUIMesh->setShininess(mMeshes[k]->getShininess());

			mpUIMesh->draw();
			mpUILight->hide();

			break;

		}

	// <-- Test if we picked a light --> //

	for (int k = 0; k < static_cast<GLint>(mLights.size()); ++k)
		if (mLights[k]->comparePickingColor(nPickedColor)){

			mPickedLight = k;

			mpUILight->setLa(mLights[k]->getLa());
			mpUILight->setLd(mLights[k]->getLd());
			mpUILight->setLs(mLights[k]->getLs());
			mpUILight->setType(mLights[k]->getType());
			mpUILight->setCutoff(mLights[k]->getCutoff());
			mpUILight->setExponent(mLights[k]->getExponent());
			mpUILight->setPosition(mLights[k]->getPosition());
			mpUILight->setDirection(mLights[k]->getDirection());

			mpUIMesh->hide();
			mpUILight->draw();

			break;

		}

	if (mPickedMesh == -1 && mPickedLight == -1){

		mpUIMesh->hide();
		mpUILight->hide();

	}

}
