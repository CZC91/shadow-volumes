#include "TextureLoader.h"

// <-- Constructor --> //

CTextureLoader::CTextureLoader(){

}

// <-- Destructor --> //

CTextureLoader::~CTextureLoader(){

}

// <-- Loads texture using DevIL --> //

GLuint CTextureLoader::load(const GLchar * nFilePath){

	ILuint texture, error;
	ILboolean success;

	ilGenImages(1, &texture);
	ilBindImage(texture);
	success = ilLoadImage((const ILstring)nFilePath);

	if (success){

		ILinfo ImageInfo;
		iluGetImageInfo(&ImageInfo);

		if (ImageInfo.Origin == IL_ORIGIN_UPPER_LEFT) iluFlipImage();

		success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);

		if (!success){

			error = ilGetError();
			printf("Image conversion failed - IL reports error: %s - %s\n", error, iluErrorString(error));
			exit(-1);

		}

		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 4);

		glTexImage2D(GL_TEXTURE_2D, 0, ilGetInteger(IL_IMAGE_FORMAT), ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT), 0, ilGetInteger(IL_IMAGE_FORMAT), GL_UNSIGNED_BYTE, ilGetData());
		glGenerateMipmap(GL_TEXTURE_2D);

		glBindTexture(GL_TEXTURE_2D, 0);

	}
	else{

		error = ilGetError();
		printf("Image conversion failed - IL reports error: %s - %s\n", error, iluErrorString(error));

	}

	return texture;

}