#ifndef LIGHT_H
#define LIGHT_H

#include <glm\glm.hpp>
#include <glm\gtx\rotate_vector.hpp>
#include <glm\gtc\matrix_transform.hpp>

#include "Drawable.h"

using glm::mat4;
using glm::vec3;
using glm::degrees;
using glm::rotateY;
using glm::translate;

namespace nLight{

	enum LightTypes{
		SPOT,
		POINT,
		DIRECTIONAL
	};

}

class CLight : public CDrawable{

private:

	GLint mType;
	mat4 mModelMatrix;
	GLfloat mCutoff, mExponent;
	vec3 mLa, mLd, mLs, mPosition, mDirection, mPickingColor;

public:

	CLight();
	~CLight();

	void draw();
	void drawBoundingBox();
	void updatePosition(const GLfloat nAngle);
	bool comparePickingColor(const vec3 nPickingColor);

	vec3 getLa();
	vec3 getLd();
	vec3 getLs();
	GLint getType();
	vec3 getPosition();
	vec3 getDirection();
	GLfloat getCutoff();
	GLfloat getExponent();
	mat4 getModelMatrix();
	vec3 getPickingColor();

	void setLa(const vec3 nLa);
	void setLd(const vec3 nLd);
	void setLs(const vec3 nLs);
	void setType(const GLint nType);
	void setCutoff(const GLfloat nCutoff);
	void setPosition(const vec3 nPosition);
	void setDirection(const vec3 nDirection);
	void setExponent(const GLfloat nExponent);
	void setPickingColor(const vec3 nPickingColor);

};

#endif