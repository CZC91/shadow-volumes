#include "Framebuffer.h"

// <-- Constructor --> //

CFramebuffer::CFramebuffer(){

}

// <-- Destructor --> //

CFramebuffer::~CFramebuffer(){

}

// <-- Get width --> //

GLuint CFramebuffer::getWidth(){

	return mWidth;

}

// <-- get height --> //

GLuint CFramebuffer::getHeight(){

	return mHeight;

}

// <-- Gets texture id --> //

GLuint CFramebuffer::getTextureId(){

	return mTexture;

}

// <-- Gets framebuffer id --> //

GLuint CFramebuffer::getFramebufferId(){

	return mFramebuffer;

}

// <-- Uses framebuffer --> //

void CFramebuffer::use(nFramebuffer::targets nTarget){

	glBindFramebuffer(nTarget, mFramebuffer);

}

// <-- Unuses framebuffer --> //

void CFramebuffer::unUse(nFramebuffer::targets nTarget){

	glBindFramebuffer(nTarget, 0);

}