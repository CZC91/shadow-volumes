#include "LoaderMesh.h"

// <-- Constructor --> //

CLoaderMesh::CLoaderMesh(){

}

// <-- Destructor --> //

CLoaderMesh::~CLoaderMesh(){

}

// <-- Parses obj and off file --> //

void CLoaderMesh::parseMeshFile(){

	string extension;
	GLchar **pTokenBuffer;
	GLuint tokenBufferSize;

	while (getline(mFile, mFileLine)){

		pTokenBuffer = getTokens(const_cast<GLchar*>(mFileLine.c_str()), " ", tokenBufferSize);

		if (!pTokenBuffer)
			continue;

		if (!strcmp(pTokenBuffer[0], "KA")){

			mKa.x = static_cast<GLfloat>(atof(pTokenBuffer[1]));
			mKa.y = static_cast<GLfloat>(atof(pTokenBuffer[2]));
			mKa.z = static_cast<GLfloat>(atof(pTokenBuffer[3]));

		}
		else if (!strcmp(pTokenBuffer[0], "KD")){

			mKd.x = static_cast<GLfloat>(atof(pTokenBuffer[1]));
			mKd.y = static_cast<GLfloat>(atof(pTokenBuffer[2]));
			mKd.z = static_cast<GLfloat>(atof(pTokenBuffer[3]));

		}
		else if (!strcmp(pTokenBuffer[0], "KS")){

			mKs.x = static_cast<GLfloat>(atof(pTokenBuffer[1]));
			mKs.y = static_cast<GLfloat>(atof(pTokenBuffer[2]));
			mKs.z = static_cast<GLfloat>(atof(pTokenBuffer[3]));

		}
		else if (!strcmp(pTokenBuffer[0], "PATH")){

			mFilePath = pTokenBuffer[1];

		}
		else if (!strcmp(pTokenBuffer[0], "SCALING")){

			mScaling.x = static_cast<GLfloat>(atof(pTokenBuffer[1]));
			mScaling.y = static_cast<GLfloat>(atof(pTokenBuffer[2]));
			mScaling.z = static_cast<GLfloat>(atof(pTokenBuffer[3]));

		}
		else if (!strcmp(pTokenBuffer[0], "ROTATION")){

			mRotation.x = static_cast<GLfloat>(atof(pTokenBuffer[1]));
			mRotation.y = static_cast<GLfloat>(atof(pTokenBuffer[2]));
			mRotation.z = static_cast<GLfloat>(atof(pTokenBuffer[3]));
			mRotation.w = static_cast<GLfloat>(atof(pTokenBuffer[4]));

		}
		else if (!strcmp(pTokenBuffer[0], "ADJACENCY")){

			mAdjacency = pTokenBuffer[1];

		}
		else if (!strcmp(pTokenBuffer[0], "EXTENSION")){

			mExtension = pTokenBuffer[1];

		}
		else if (!strcmp(pTokenBuffer[0], "SHININESS")){

			mShininess = static_cast<GLfloat>((atof(pTokenBuffer[1])));

		}
		else if (!strcmp(pTokenBuffer[0], "POSITION")){

			mPosition.x = static_cast<GLfloat>(atof(pTokenBuffer[1]));
			mPosition.y = static_cast<GLfloat>(atof(pTokenBuffer[2]));
			mPosition.z = static_cast<GLfloat>(atof(pTokenBuffer[3]));

		}
		else if (!strcmp(pTokenBuffer[0], "PICKING_COLOR")){

			mPickingColor.x = static_cast<GLfloat>(atof(pTokenBuffer[1]));
			mPickingColor.y = static_cast<GLfloat>(atof(pTokenBuffer[2]));
			mPickingColor.z = static_cast<GLfloat>(atof(pTokenBuffer[3]));

		}
		else if (!strcmp(pTokenBuffer[0], "TEXTURE_PATH")){

			mTexturePath = pTokenBuffer[1];

		}
		else if (!strcmp(pTokenBuffer[0], "END")){

			break;

		}

	}

	if (!strcmp(mExtension.c_str(), "OBJ")){

		mpLoaderObj = new CLoaderObj();
		mpLoaderObj->load(mFilePath.c_str());

		mIndexes = mpLoaderObj->getIndexes();
		mNormals = mpLoaderObj->getNormals();
		mVertexes = mpLoaderObj->getVertexes();
		mBoundingBox = mpLoaderObj->getBoundingBox();
		mBoundingBoxMid = mpLoaderObj->getBoundingBoxMid();
		mTextureCoordinates = mpLoaderObj->getTextureCoordinates();

		delete mpLoaderObj;

	}
	else if (!strcmp(mExtension.c_str(), "OFF")){

		mpLoaderOff = new CLoaderOff();
		mpLoaderOff->load(mFilePath.c_str());

		mIndexes = mpLoaderOff->getIndexes();
		mVertexes = mpLoaderOff->getVertexes();
		mBoundingBox = mpLoaderOff->getBoundingBox();
		mBoundingBoxMid = mpLoaderOff->getBoundingBoxMid();
		
		delete mpLoaderOff;

	}

}

// <-- Creates a new mesh --> //

CMesh * CLoaderMesh::createMesh(){

	GLuint textureID;
	CMesh *pMeshSource = new CMesh();

	pMeshSource->setKa(mKa);
	pMeshSource->setKd(mKd);
	pMeshSource->setKs(mKs);
	pMeshSource->setScaling(mScaling);
	pMeshSource->setPosition(mPosition);
	pMeshSource->setRotation(mRotation);
	pMeshSource->setShininess(mShininess);
	pMeshSource->setMidPoint(mBoundingBoxMid);
	pMeshSource->setPickingColor(mPickingColor);

	if (strcmp(mTexturePath.c_str(), "")){

		mpTextureLoader = new CTextureLoader();
		textureID = mpTextureLoader->load(const_cast<GLchar*>(mTexturePath.c_str()));
		pMeshSource->setTextureID(textureID);
		delete mpTextureLoader;

	}

	if (!strcmp(mAdjacency.c_str(), "TRUE")){

		printf("Shadow caster\n");
		computeAdjacency();
		pMeshSource->setAdjacency(true);
		pMeshSource->setShadowCaster(true);

	}
	else{

		printf("No shadow caster\n");
		pMeshSource->setShadowCaster(false);

	}
		
	pMeshSource->fillBuffer(nBuffer::NORMALS_BUFFER, mNormals.size(), mNormals.data());
	pMeshSource->fillBuffer(nBuffer::VERTEXES_3D_BUFFER, mVertexes.size(), mVertexes.data());
	pMeshSource->fillBuffer(nBuffer::VERTEXES_INDEXES_BUFFER, mIndexes.size(), mIndexes.data());
	pMeshSource->fillBuffer(nBuffer::BOUNDING_BOX_BUFFER, mBoundingBox.size(), mBoundingBox.data());
	pMeshSource->fillBuffer(nBuffer::TEXTURE_COORDINATES_BUFFER, mTextureCoordinates.size(), mTextureCoordinates.data());

	return pMeshSource;

}

// <-- Loads a set of mesh files --> //

vector<CMesh*> CLoaderMesh::load(const GLchar *npFilePath){

	GLchar **pTokenBuffer;
	GLuint tokenBufferSize;
	vector<CMesh*> meshes;

	mFile.open(npFilePath, ios::in);

	while (getline(mFile, mFileLine)){

		pTokenBuffer = getTokens(const_cast<GLchar*>(mFileLine.c_str()), " ", tokenBufferSize);

		if (!pTokenBuffer)
			continue;

		if (!strcmp(pTokenBuffer[0], "MESH_FILE")){

			parseMeshFile();

		}

		meshes.push_back(createMesh());

		clear();

	}

	mFile.close();

	return meshes;

}
