#include "Main.h"

namespace nMain{

	CScene *gpScene;
	CCamera *gpCamera;
	GLuint gFpsCounter;
	GLFWwindow *gpWindow;
	CGPUProperties *gpGPUProperties;
	GLfloat gCurrentTime, gPreviousTime;
	CFramebufferColor *gpFramebufferColor;
	ivec2 gViewportDimensions, gMousePosition;

};

// <-- Inits application --> //

void initApp(){

	// <-- Init Z-buffering --> //

	glEnable(GL_DEPTH_TEST);

	// <-- Init DevIL --> //

	ilutRenderer(ILUT_OPENGL);
	ilInit();
	iluInit();
	ilutInit();

	// <-- Init AntTweakBar --> //

	if (!TwInit(TW_OPENGL_CORE, NULL)){

		glfwTerminate();
		return;

	}

	TwWindowSize(nMain::gViewportDimensions.x, nMain::gViewportDimensions.y);

	nMain::gpScene = new CScene();
	nMain::gpScene->init();

	// <-- Init Camera --> //

	nCamera::gMoveSpeed = 1.0f;
	nCamera::gRotationSpeed = 0.2f;

	nMain::gpCamera = new CCamera();
	nMain::gpCamera->setView(vec3(50.0f, 50.0f, 50.0f), vec3(-0.5f, -0.5f, -0.5f), vec3(0.0f, 1.0f, 0.0f));
	nMain::gpCamera->setProjection(70.0f, static_cast<GLfloat>(nMain::gViewportDimensions.x) / static_cast<GLfloat>(nMain::gViewportDimensions.y), 0.1f, 1000.0f);

	// <-- Inits color framebuffer --> //

	nMain::gpFramebufferColor = new CFramebufferColor();
	nMain::gpFramebufferColor->init(nMain::gViewportDimensions.x, nMain::gViewportDimensions.y);

	// <-- Inits scene shadow framebuffer --> //

	nScene::gpFramebufferShadow = new CFramebufferShadow();
	nScene::gpFramebufferShadow->init(nMain::gViewportDimensions.x, nMain::gViewportDimensions.y);

}

// <-- Clears application --> //

void clearApp(){

	delete nMain::gpScene;
	delete nMain::gpCamera;
	delete nMain::gpFramebufferColor;
	glfwDestroyWindow(nMain::gpWindow);

}

// <-- Inits GLFW --> //

bool initGLFW(){

	if (!glfwInit()){

		glfwTerminate();
		return false;

	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	nMain::gViewportDimensions.x = 1200;
	nMain::gViewportDimensions.y = 800;

	nMain::gpWindow = glfwCreateWindow(nMain::gViewportDimensions.x, nMain::gViewportDimensions.y, "Shadow Volumes", NULL, NULL);

	if (!nMain::gpWindow){

		glfwTerminate();
		return false;

	}

	glfwMakeContextCurrent(nMain::gpWindow);
	glfwSetCharCallback(nMain::gpWindow, charCallback);
	glfwSetKeyCallback(nMain::gpWindow, keyboardCallback);
	glfwSetCursorPosCallback(nMain::gpWindow, mouseMoveCallback);
	glfwSetMouseButtonCallback(nMain::gpWindow, mouseDownCallback);
	glfwSetWindowSizeCallback(nMain::gpWindow, windowResizeCallback);

	return true;

}

// <-- Inits OpenGL core --> //

bool initCore(){

	if (ogl_LoadFunctions() == ogl_LOAD_FAILED){

		glfwTerminate();
		return false;

	}

	nMain::gpGPUProperties = new CGPUProperties();

	printf("Vendor : %s\n", nMain::gpGPUProperties->getVendor().c_str());
	printf("Renderer : %s\n", nMain::gpGPUProperties->getRenderer().c_str());
	printf("GLSL Version : %s\n", nMain::gpGPUProperties->getGLSLVersion().c_str());
	printf("OpenGL Version : %s\n", nMain::gpGPUProperties->getOpenGLVersion().c_str());
	
	return true;

}

// <-- Application main loop --> //

void mainLoop(){

	while (!glfwWindowShouldClose(nMain::gpWindow) && !glfwGetKey(nMain::gpWindow, GLFW_KEY_ESCAPE)){

		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
		
		updateApp();

		nMain::gpScene->draw();

		TwDraw();

		glfwSwapBuffers(nMain::gpWindow);
		glfwPollEvents();

		updateFPS();

	}

}

// <-- Updates application--> //

void updateApp(){
	
	nScene::gEyePosition = nMain::gpCamera->getPosition();
	nScene::gViewMatrix = nMain::gpCamera->getViewMatrix();
	nScene::gProjectionMatrix = nMain::gpCamera->getProjectionMatrix();
	nScene::gInfinitePerspectiveProjectionMatrix = infinitePerspective(nMain::gpCamera->getFov(), nMain::gpCamera->getAspectRatio(), nMain::gpCamera->getPlaneDistance(nCamera::NEAR_PLANE));
	
	string title = "Shadow Volumes - " + nMain::gpGPUProperties->getRenderer() + " - FPS " + to_string(nMain::gFpsCounter);
	glfwSetWindowTitle(nMain::gpWindow, const_cast<GLchar*>(title.c_str()));

}

// <-- Updates frames per second --> //

void updateFPS(){

	GLfloat currentTime;
	static GLfloat previousTime = 0;
	static GLfloat timeInterval = 0;
	static GLint frameCount = 0;

	frameCount++;

	currentTime = static_cast<GLfloat>(glfwGetTime());
	timeInterval = currentTime - previousTime;

	nScene::gAngle =  0.01f;
	if (nScene::gAngle > 2.0 * PI) nScene::gAngle -= static_cast<GLfloat>(2.0 * PI);

	if (timeInterval > 1.0f){

		nMain::gFpsCounter = int(frameCount / timeInterval);
		previousTime = currentTime;
		frameCount = 0;

	}

}

// <-- Picks a new color from framebuffer --> //

vec3 pickColor(const GLuint nX, const GLuint nY){

	GLubyte colors[4];
	vec3 pickedColor;

	nMain::gpFramebufferColor->use(nFramebuffer::DEFAULT_TARGET);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(0.3f, 0.3f, 0.3f, 1.0f);

		nMain::gpScene->drawPick();
		glReadPixels(nX, nMain::gViewportDimensions.y - nY, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, colors);

	nMain::gpFramebufferColor->unUse(nFramebuffer::DEFAULT_TARGET);

	pickedColor.x = colors[0];
	pickedColor.y = colors[1];
	pickedColor.z = colors[2];

	return pickedColor;

}

// <-- Char callback --> //

void charCallback(GLFWwindow *npWindow, GLuint nChar){

	if (TwEventCharGLFW(nChar, GLFW_PRESS))
		return;

}

// <-- Mouse move callback --> //

void mouseMoveCallback(GLFWwindow *npWindow, GLdouble nX, GLdouble nY){

	GLuint castedX, castedY;

	castedX = static_cast<GLuint>(nX);
	castedY = static_cast<GLuint>(nY);

	if (!TwEventMousePosGLFW(castedX, castedY))
		nMain::gpCamera->onMouseMove(castedX, castedY);

	nMain::gMousePosition.x = castedX;
	nMain::gMousePosition.y = castedY;
		
}

// <-- Window resize callback --> //

void windowResizeCallback(GLFWwindow *npWindow, GLint nWidth, GLint nHeight){

	nMain::gViewportDimensions.x = nWidth;
	nMain::gViewportDimensions.y = nHeight;

	TwWindowSize(nMain::gViewportDimensions.x, nMain::gViewportDimensions.y);

	delete nMain::gpFramebufferColor;

	nMain::gpFramebufferColor = new CFramebufferColor();
	nMain::gpFramebufferColor->init(nMain::gViewportDimensions.x, nMain::gViewportDimensions.y);

	nScene::gpFramebufferShadow = new CFramebufferShadow();
	nScene::gpFramebufferShadow->init(nMain::gViewportDimensions.x, nMain::gViewportDimensions.y);

	glViewport(0, 0, nMain::gViewportDimensions.x, nMain::gViewportDimensions.y);

}

// <-- Mouse down callback --> //

void mouseDownCallback(GLFWwindow *npWindow, GLint nButton, GLint nActions, GLint nMods){

	if (!TwEventMouseButtonGLFW(nButton, nActions)){

		if (nActions == GLFW_PRESS){

			if (nButton == GLFW_MOUSE_BUTTON_LEFT){

				if (nScene::gPickingMode) {

					nMain::gpScene->checkPicking(pickColor(nMain::gMousePosition.x, nMain::gMousePosition.y));
					nScene::gPickingMode = false;

				}else nMain::gpCamera->setDragging(true);
			
			}
			else if (nButton == GLFW_MOUSE_BUTTON_RIGHT){

				nScene::gPickingMode = true;

			}

		}
		else{

			nMain::gpCamera->setDragging(false);

		}

	}

}

// <-- Keyboard callback --> //

void keyboardCallback(GLFWwindow *npWindow, GLint nKey, GLint nScanCode, GLint nActions, GLint nMods){

	if (!TwEventKeyGLFW(nKey, nActions)){

		if (glfwGetKey(nMain::gpWindow, GLFW_KEY_W))

			nMain::gpCamera->onKeyDown(nCamera::VK_W);

		else if (glfwGetKey(nMain::gpWindow, GLFW_KEY_S))

			nMain::gpCamera->onKeyDown(nCamera::VK_S);

		else if (glfwGetKey(nMain::gpWindow, GLFW_KEY_A))

			nMain::gpCamera->onKeyDown(nCamera::VK_A);

		else if (glfwGetKey(nMain::gpWindow, GLFW_KEY_D))

			nMain::gpCamera->onKeyDown(nCamera::VK_D);

		else if (glfwGetKey(nMain::gpWindow, GLFW_KEY_Q))

			nMain::gpCamera->onKeyDown(nCamera::VK_Q);

		else if (glfwGetKey(nMain::gpWindow, GLFW_KEY_E))

			nMain::gpCamera->onKeyDown(nCamera::VK_E);

	}

}

// <-- Main --> //

void main(int argc, char ** argv){

	if (initGLFW() && initCore()){

		initApp();
		mainLoop();

	}

	clearApp();

}