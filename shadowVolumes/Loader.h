#ifndef LOADER_H
#define LOADER_H

#include <string>
#include <vector>
#include <cstdlib>
#include <fstream>
#include <glm\glm.hpp>
#include <Core\gl_core_4_3.h>

#ifndef ABS
#define ABS(a) (((a)>=(0))?(a):(-a))
#endif

#ifndef MIN
#define MIN(a,b) (((a)<(b))?(a):(b))
#endif

#ifndef MAX
#define MAX(a,b) (((a)>(b))?(a):(b))
#endif

using std::ios;
using glm::vec2;
using glm::vec3;
using glm::ivec3;
using std::string;
using std::vector;
using std::fstream;

class CLoader{

protected:

	fstream mFile;
	vector<GLuint> mIndexes;
	string mFilePath, mFileLine;
	vec3 mBoundingBoxMax, mBoundingBoxMid, mBoundingBoxMin;
	vector<GLfloat> mNormals, mVertexes, mTangents, mBinormals, mBoundingBox, mTextureCoordinates;

	void computeNormals();
	void computeTangents();
	void computeAdjacency();
	void computeBoundingBox();
	GLchar ** getTokens(GLchar *nLine, const GLchar * nDelimeters, GLuint &nBufferSize);

public:

	CLoader();
	~CLoader();

	void clear();
	vec3 getBoundingBoxMax();
	vec3 getBoundingBoxMid();
	vec3 getBoundingBoxMin();
	vector<GLuint> getIndexes();
	vector<GLfloat> getNormals();
	vector<GLfloat> getVertexes();
	vector<GLfloat> getTangents();
	vector<GLfloat> getBinormals();
	vector<GLfloat> getBoundingBox();
	vector<GLfloat> getTextureCoordinates();

};

#endif