#ifndef LOADER_OFF_H
#define LOADER_OFF_H

#include "Loader.h"

class CLoaderOff : public CLoader{

private:

	void initBoundingBoxMax();
	void initBoundingBoxMin();
	void parseIndexes(const GLuint nLines);
	void parseVertexes(const GLuint nLines);
	void updateBoundingBoxMax(const GLfloat x, const GLfloat y, const GLfloat z);
	void updateBoundingBoxMin(const GLfloat x, const GLfloat y, const GLfloat z);

public:

	CLoaderOff();
	~CLoaderOff();
	void load(const GLchar *nFilePath);

};

#endif
