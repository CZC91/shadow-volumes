#include "FramebufferColor.h"

#include <cstdio>

// <-- Constructor --> //

CFramebufferColor::CFramebufferColor(){

}

// <-- Destructor --> //

CFramebufferColor::~CFramebufferColor(){

	glDeleteTextures(1, &mTexture);
	glDeleteFramebuffers(1, &mFramebuffer);
	glDeleteRenderbuffers(1, &mRenderbuffer);

}

// <-- Inits color framebuffer --> //

void CFramebufferColor::init(const GLuint nWidth, const GLuint nHeight){

	mWidth = nWidth;
	mHeight = nHeight;

	glGenTextures(1, &mTexture);
	glBindTexture(GL_TEXTURE_2D, mTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, mWidth, mHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glBindTexture(GL_TEXTURE_2D, 0);

	glGenRenderbuffers(1, &mRenderbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, mRenderbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, mWidth, mHeight);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	glGenFramebuffers(1, &mFramebuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, mFramebuffer);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, mTexture, NULL);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, mRenderbuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

}
