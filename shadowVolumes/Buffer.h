#ifndef BUFFER_H
#define BUFFER_H

#include <Core\gl_core_4_3.h>
#include "AppException.h"

#define BUFFER_OFFSET(nOffset)((void*)(nOffset))

namespace nBuffer{

	enum id{
		NORMALS_BUFFER,
		VERTEXES_3D_BUFFER,
		BOUNDING_BOX_BUFFER,
		VERTEXES_INDEXES_BUFFER,
		TEXTURE_COORDINATES_BUFFER
	};

	enum target{
		ARRAY_BUFFER = GL_ARRAY_BUFFER,
		TEXTURE_BUFFER = GL_TEXTURE_BUFFER,
		UNIFORM_BUFFER = GL_UNIFORM_BUFFER,
		COPY_READ_BUFFER = GL_COPY_READ_BUFFER,
		COPY_WRITE_BUFFER = GL_COPY_WRITE_BUFFER,
		PIXEL_PACK_BUFFER = GL_PIXEL_PACK_BUFFER,
		PIXEL_UNPACK_BUFFER = GL_PIXEL_UNPACK_BUFFER,
		ELEMENT_ARRAY_BUFFER = GL_ELEMENT_ARRAY_BUFFER,
		TRANSFORM_FEEDBACK_BUFFER = GL_TRANSFORM_FEEDBACK_BUFFER
	};

	enum use{
		STREAM_COPY = GL_STREAM_COPY,
		STREAM_DRAW = GL_STREAM_DRAW,
		STREAM_READ = GL_STREAM_READ,
		STATIC_COPY = GL_STATIC_COPY,
		STATIC_DRAW = GL_STATIC_DRAW,
		STATIC_READ = GL_STATIC_READ,
		DYNAMIC_COPY = GL_DYNAMIC_COPY,
		DYNAMIC_DRAW = GL_DYNAMIC_DRAW,
		DYNAMIC_READ = GL_DYNAMIC_READ
	};

	enum layout{
		NORMALS_LAYOUT,
		VERTEXES_3D_LAYOUT,
		BOUNDING_BOX_LAYOUT,
		VERTEXES_INDEXES_LAYOUT,
		TEXTURE_COORDINATES_LAYOUT
	};

	enum size{
		NORMALS_SIZE = 3,
		INDEXES_SIZE = 1,
		VERTEXES_3D_SIZE = 3,
		TEXTURE_COORDINATES_SIZE = 2
	};

	struct bufferProperties{
		nBuffer::id mId;
		nBuffer::use mUse;
		nBuffer::size mSize;
		nBuffer::target mTarget;
		nBuffer::layout mLayout;
	};

	extern bufferProperties mBufferProperties[5];

};

class CBuffer{

private:

	bool mAdjacency;
	GLuint mVAO, mVBO[5], mVBOSize[5], mNumberOfBuffers;

public:

	CBuffer();
	~CBuffer();

	void draw();
	void drawBoundingBox();
	void setAdjacency(const bool nAdjacency);
	void fillBuffer(nBuffer::id, const GLuint nSize, void *npData);

};

#endif