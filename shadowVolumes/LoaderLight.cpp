#include "LoaderLight.h"

// <-- Constructor --> //

CLoaderLight::CLoaderLight(){

}

// <-- Destructor --> //

CLoaderLight::~CLoaderLight(){

}

// <-- Parses a new light source --> //

void CLoaderLight::parseLight(){

	GLchar **pTokenBuffer;
	GLuint tokenBufferSize;

	while (getline(mFile, mFileLine)){

		pTokenBuffer = getTokens(const_cast<GLchar*>(mFileLine.c_str()), " ", tokenBufferSize);

		if (!pTokenBuffer)
			continue;

		if (!strcmp(pTokenBuffer[0], "LA")){

			mLa.x = static_cast<GLfloat>(atof(pTokenBuffer[1]));
			mLa.y = static_cast<GLfloat>(atof(pTokenBuffer[2]));
			mLa.z = static_cast<GLfloat>(atof(pTokenBuffer[3]));

		}
		else if (!strcmp(pTokenBuffer[0], "LD")){

			mLd.x = static_cast<GLfloat>(atof(pTokenBuffer[1]));
			mLd.y = static_cast<GLfloat>(atof(pTokenBuffer[2]));
			mLd.z = static_cast<GLfloat>(atof(pTokenBuffer[3]));

		}
		else if (!strcmp(pTokenBuffer[0], "LS")){

			mLs.x = static_cast<GLfloat>(atof(pTokenBuffer[1]));
			mLs.y = static_cast<GLfloat>(atof(pTokenBuffer[2]));
			mLs.z = static_cast<GLfloat>(atof(pTokenBuffer[3]));

		}
		else if (!strcmp(pTokenBuffer[0], "PATH")){

			mFilePath = pTokenBuffer[1];

		}
		else if (!strcmp(pTokenBuffer[0], "CUTOFF")){

			mCutoff = static_cast<GLfloat>(atof(pTokenBuffer[1]));

		}
		else if (!strcmp(pTokenBuffer[0], "EXPONENT")){

			mExponent = static_cast<GLfloat>(atof(pTokenBuffer[1]));

		}
		else if (!strcmp(pTokenBuffer[0], "POSITION")){

			mPosition.x = static_cast<GLfloat>(atof(pTokenBuffer[1]));
			mPosition.y = static_cast<GLfloat>(atof(pTokenBuffer[2]));
			mPosition.z = static_cast<GLfloat>(atof(pTokenBuffer[3]));

		}
		else if (!strcmp(pTokenBuffer[0], "DIRECTION")){

			mDirection.x = static_cast<GLfloat>(atof(pTokenBuffer[1]));
			mDirection.y = static_cast<GLfloat>(atof(pTokenBuffer[2]));
			mDirection.z = static_cast<GLfloat>(atof(pTokenBuffer[3]));

		}
		else if (!strcmp(pTokenBuffer[0], "PICKING_COLOR")){

			mPickingColor.x = static_cast<GLfloat>(atof(pTokenBuffer[1]));
			mPickingColor.y = static_cast<GLfloat>(atof(pTokenBuffer[2]));
			mPickingColor.z = static_cast<GLfloat>(atof(pTokenBuffer[3]));

		}
		else if (!strcmp(pTokenBuffer[0], "END")){

			break;

		}

	}

	mpLoaderOff = new CLoaderOff();
	mpLoaderOff->load(mFilePath.c_str());

	mIndexes = mpLoaderOff->getIndexes();
	mVertexes = mpLoaderOff->getVertexes();
	mBoundingBox = mpLoaderOff->getBoundingBox();
	mBoundingBoxMid = mpLoaderOff->getBoundingBoxMid();

	delete mpLoaderOff;

}

// <-- Creates a new light source --> //

CLight * CLoaderLight::createLight(){

	CLight *pLightSource = new CLight();

	pLightSource->setLa(mLa);
	pLightSource->setLd(mLd);
	pLightSource->setLs(mLs);
	pLightSource->setType(mType);
	pLightSource->setCutoff(mCutoff);
	pLightSource->setExponent(mExponent);
	pLightSource->setPosition(mPosition);
	pLightSource->setDirection(mDirection);
	pLightSource->setPickingColor(mPickingColor);

	pLightSource->fillBuffer(nBuffer::VERTEXES_3D_BUFFER, mVertexes.size(), mVertexes.data());
	pLightSource->fillBuffer(nBuffer::VERTEXES_INDEXES_BUFFER, mIndexes.size(), mIndexes.data());
	pLightSource->fillBuffer(nBuffer::BOUNDING_BOX_BUFFER, mBoundingBox.size(), mBoundingBox.data());

	return pLightSource;

}

// <-- Loads a set of lights --> //

vector<CLight*> CLoaderLight::load(const char* npFilePath){

	GLchar **pTokenBuffer;
	GLuint tokenBufferSize;
	vector<CLight*> lights;

	mFile.open(npFilePath, ios::in);

	while (getline(mFile, mFileLine)){

		pTokenBuffer = getTokens(const_cast<GLchar*>(mFileLine.c_str()), " ", tokenBufferSize);

		if (!pTokenBuffer) 
			continue;

		if (!strcmp(pTokenBuffer[0], "SPOT_LIGHT"))
			mType = nLoaderLight::SPOT;
		
		else if (!strcmp(pTokenBuffer[0], "POINT_LIGHT"))
			mType = nLoaderLight::POINT;

		else if (!strcmp(pTokenBuffer[0], "DIRECTIONAL_LIGHT"))
			mType = nLoaderLight::DIRECTIONAL;

		parseLight();
		lights.push_back(createLight());

		clear();

	}

	mFile.close();

	return lights;

}
