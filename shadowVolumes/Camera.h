#ifndef CAMERA_H
#define CAMERA_H

#include <glm\glm.hpp>
#include <Core\gl_core_4_3.h>
#include <glm\gtx\euler_angles.hpp>
#include <glm\gtc\matrix_transform.hpp>

#ifndef PI
#define PI acos(-1)
#endif

using glm::mat3;
using glm::mat4;
using glm::vec2;
using glm::vec3;
using glm::vec4;

using glm::cross;
using glm::length;
using glm::lookAt;
using glm::rotate;
using glm::frustum;
using glm::radians;
using glm::translate;
using glm::perspective;
using glm::yawPitchRoll;

namespace nCamera{

	enum movementKeys{
		VK_W = 0x57,
		VK_S = 0x53,
		VK_A = 0x41,
		VK_D = 0x44,
		VK_Q = 0x51,
		VK_E = 0x45
	};

	enum frustumPlanes{
		FAR_PLANE,
		LEFT_PLANE,
		NEAR_PLANE,
		RIGHT_PLANE,
		UPPER_PLANE,
		BOTTOM_PLANE
	};

	enum projectionModes{
		PERSPECTIVE,
		ORTHOGRAPHIC
	};

	extern GLfloat gMoveSpeed, gRotationSpeed;

};

class CCamera{

private:

	bool mDrag;
	vec2 mMouseOldPosition, mMouseDelta;
	GLfloat mFov, mAspectRatio, mPlanes[6];
	vec3 mPosition, mViewVector, mTargetPoint, mUpVector, mRightVector;

	void lift(const GLfloat nSpeed);
	void walk(const GLfloat nSpeed);
	void strafe(const GLfloat nSpeed);

public:

	CCamera();
	~CCamera();

	void onKeyDown(const nCamera::movementKeys nKey);
	void onMouseMove(const GLuint nMousePositionX, const GLuint nMousePositionY);

	GLfloat getFov();
	bool isDragging();
	vec3 getPosition();
	vec3 getUpVector();
	mat4 getViewMatrix();
	vec3 getViewVector();
	vec3 getTargetPoint();
	GLfloat getAspectRatio();
	mat4 getProjectionMatrix();
	GLfloat getPlaneDistance(const nCamera::frustumPlanes nPlane);

	void setFov(const GLfloat nFov);
	void setDragging(const bool nDrag);
	void setPosition(const vec3 nPosition);
	void setUpVector(const vec3 nUpVector);
	void setViewVector(const vec3 nViewVector);
	void setAspectRatio(const GLfloat nAspectRatio);
	void setView(const vec3 nPosition, const vec3 nViewVector, const vec3 nUpVector);
	void setPlaneDistance(const nCamera::frustumPlanes nPlane, const GLfloat nDistance);
	void setProjection(const GLfloat nFov, const GLfloat nAspectRatio, const GLfloat nNearPlane, const GLfloat nFarPlane);

};

#endif