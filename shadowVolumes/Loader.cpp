#include "Loader.h"

// <-- Constructor --> //

CLoader::CLoader(){

}

// <-- Destructor --> //

CLoader::~CLoader(){

}

// <-- Clear all data vectors --> //

void CLoader::clear(){

	mIndexes.clear();
	mNormals.clear();
	mVertexes.clear();
	mTangents.clear();
	mBinormals.clear();
	mBoundingBox.clear();
	mTextureCoordinates.clear();

}

// <-- Computes average normals --> //

void CLoader::computeNormals(){

	GLuint x, y, z;

	vec3 normal, point0, point1, point2;

	GLfloat *avgN = new GLfloat[(GLint)mVertexes.size()];
	GLuint *avgV = new GLuint[(GLint)mVertexes.size() / 3];

	memset(avgN, 0, mVertexes.size() * sizeof(GLfloat));
	memset(avgV, 0, mVertexes.size() * sizeof(GLfloat) / 3);

	for (int k = 0; k < (int)mIndexes.size(); k += 3){

		x = 3 * mIndexes[k + 0];
		y = 3 * mIndexes[k + 1];
		z = 3 * mIndexes[k + 2];

		point0 = vec3(mVertexes[x + 0], mVertexes[x + 1], mVertexes[x + 2]);
		point1 = vec3(mVertexes[y + 0], mVertexes[y + 1], mVertexes[y + 2]);
		point2 = vec3(mVertexes[z + 0], mVertexes[z + 1], mVertexes[z + 2]);

		normal = glm::cross(point1 - point0, point2 - point0);
		normal = (normal.x != 0 || normal.y != 0 || normal.z != 0) ? glm::normalize(normal) : normal;

		avgN[x + 0] += normal.x;
		avgN[x + 1] += normal.y;
		avgN[x + 2] += normal.z;

		avgN[y + 0] += normal.x;
		avgN[y + 1] += normal.y;
		avgN[y + 2] += normal.z;

		avgN[z + 0] += normal.x;
		avgN[z + 1] += normal.y;
		avgN[z + 2] += normal.z;

		++avgV[x / 3]; ++avgV[y / 3]; ++avgV[z / 3];

	}

	for (int k = 0; k < (int)mVertexes.size() / 3; ++k){

		x = 3 * k + 0;
		y = 3 * k + 1;
		z = 3 * k + 2;

		mNormals.push_back(static_cast<GLfloat>(avgN[x] / avgV[k]));
		mNormals.push_back(static_cast<GLfloat>(avgN[y] / avgV[k]));
		mNormals.push_back(static_cast<GLfloat>(avgN[z] / avgV[k]));

	}

	delete avgN;
	delete avgV;

}

// <-- Computes tangents and binormals --> //

void CLoader::computeTangents(){

	GLfloat R, handedness;

	GLuint x, y, z;

	vector<GLfloat> temporalTangent;
	vector<GLfloat> temporalBinormal;

	vec2 texPoint0, texPoint1, texPoint2, S, T;
	vec3 point0, point1, point2, vector0, vector1, normal, tangent, binormal;

	for (int k = 0; k < (int)mVertexes.size() / 3; ++k){

		temporalTangent.push_back(0.0);
		temporalTangent.push_back(0.0);
		temporalTangent.push_back(0.0);

		temporalBinormal.push_back(0.0);
		temporalBinormal.push_back(0.0);
		temporalBinormal.push_back(0.0);

	}

	for (int k = 0; k < (int)mIndexes.size(); k += 3){

		x = mIndexes[k + 0];
		y = mIndexes[k + 1];
		z = mIndexes[k + 2];

		point0 = vec3(mVertexes[3 * x + 0], mVertexes[3 * x + 1], mVertexes[3 * x + 2]);
		point1 = vec3(mVertexes[3 * y + 0], mVertexes[3 * y + 1], mVertexes[3 * y + 2]);
		point2 = vec3(mVertexes[3 * z + 0], mVertexes[3 * z + 1], mVertexes[3 * z + 2]);

		texPoint0 = vec2(mTextureCoordinates[2 * x + 0], mTextureCoordinates[2 * x + 1]);
		texPoint1 = vec2(mTextureCoordinates[2 * y + 0], mTextureCoordinates[2 * y + 1]);
		texPoint2 = vec2(mTextureCoordinates[2 * z + 0], mTextureCoordinates[2 * z + 1]);

		vector0 = point1 - point0;
		vector1 = point2 - point0;

		S = vec2(texPoint1.x - texPoint0.x, texPoint2.x - texPoint0.x);
		T = vec2(texPoint1.y - texPoint0.y, texPoint2.y - texPoint0.y);

		R = 1.0f / (S.x * T.y - S.y * T.x);

		tangent.x = (T.y * vector0.x - T.x * vector1.x) * R;
		tangent.y = (T.y * vector0.y - T.x * vector1.y) * R;
		tangent.z = (T.y * vector0.z - T.x * vector1.z) * R;

		binormal.x = (S.x * vector1.x - S.y * vector0.x) * R;
		binormal.y = (S.x * vector1.y - S.y * vector0.y) * R;
		binormal.z = (S.x * vector1.z - S.y * vector0.z) * R;

		temporalTangent[3 * x + 0] += tangent.x;
		temporalTangent[3 * y + 0] += tangent.x;
		temporalTangent[3 * z + 0] += tangent.x;

		temporalTangent[3 * x + 1] += tangent.y;
		temporalTangent[3 * y + 1] += tangent.y;
		temporalTangent[3 * z + 1] += tangent.y;

		temporalTangent[3 * x + 2] += tangent.z;
		temporalTangent[3 * y + 2] += tangent.z;
		temporalTangent[3 * z + 2] += tangent.z;

		temporalBinormal[3 * x + 0] += binormal.x;
		temporalBinormal[3 * y + 0] += binormal.x;
		temporalBinormal[3 * z + 0] += binormal.x;

		temporalBinormal[3 * x + 1] += binormal.y;
		temporalBinormal[3 * y + 1] += binormal.y;
		temporalBinormal[3 * z + 1] += binormal.y;

		temporalBinormal[3 * x + 2] += binormal.z;
		temporalBinormal[3 * y + 2] += binormal.z;
		temporalBinormal[3 * z + 2] += binormal.z;

	}

	for (int k = 0; k < (int)mVertexes.size() / 3; ++k){

		x = 3 * k + 0;
		y = 3 * k + 1;
		z = 3 * k + 2;

		normal = vec3(mNormals[x], mNormals[y], mNormals[z]);
		tangent = vec3(temporalTangent[x], temporalTangent[y], temporalTangent[z]);
		binormal = vec3(temporalBinormal[x], temporalBinormal[y], temporalBinormal[z]);

		tangent = vec3(glm::normalize(tangent - glm::dot(normal, tangent) * normal));
		handedness = (glm::dot(glm::cross(normal, tangent), binormal) < 0.0f) ? -1.0f : 1.0f;

		mTangents.push_back(tangent.x);
		mTangents.push_back(tangent.y);
		mTangents.push_back(tangent.z);
		mTangents.push_back(handedness);

		mBinormals.push_back(binormal.x);
		mBinormals.push_back(binormal.y);
		mBinormals.push_back(binormal.z);

	}

}

// <-- Computes adjacent triangles --> //

void CLoader::computeAdjacency(){

	ivec3 triangle0, triangle1;
	vector<GLuint> adjacentTriangles;

	for (int k = 0; k < static_cast<GLint>(mIndexes.size()); k += 3){

		adjacentTriangles.push_back(mIndexes[k + 0]);
		adjacentTriangles.push_back(-1);
		adjacentTriangles.push_back(mIndexes[k + 1]);
		adjacentTriangles.push_back(-1);
		adjacentTriangles.push_back(mIndexes[k + 2]);
		adjacentTriangles.push_back(-1);

	}

	for (int k = 0; k < static_cast<GLint>(adjacentTriangles.size()); k += 6){

		triangle0.x = adjacentTriangles[k + 0];
		triangle0.y = adjacentTriangles[k + 2];
		triangle0.z = adjacentTriangles[k + 4];

		for (int t = k + 6; t < static_cast<GLint>(adjacentTriangles.size()); t += 6){

			triangle1.x = adjacentTriangles[t + 0];
			triangle1.y = adjacentTriangles[t + 2];
			triangle1.z = adjacentTriangles[t + 4];

			// <-- Edge #1 == Edge #1 --> //

			if ((triangle0.x == triangle1.x && triangle0.y == triangle1.y) || (triangle0.x == triangle1.y && triangle0.y == triangle1.x)){

				adjacentTriangles[k + 1] = triangle1.z;
				adjacentTriangles[t + 1] = triangle0.z;

			}

			// <-- Edge #1 == Edge #2 --> //

			if ((triangle0.x == triangle1.y && triangle0.y == triangle1.z) || (triangle0.x == triangle1.z && triangle0.y == triangle1.y)){

				adjacentTriangles[k + 1] = triangle1.x;
				adjacentTriangles[t + 3] = triangle0.z;

			}

			// <-- Edge #1 == Edge #3 --> //

			if ((triangle0.x == triangle1.z && triangle0.y == triangle1.x) || (triangle0.x == triangle1.x && triangle0.y == triangle1.z)){

				adjacentTriangles[k + 1] = triangle1.y;
				adjacentTriangles[t + 5] = triangle0.z;

			}

			// <-- Edge #2 == Edge #1 --> //

			if ((triangle0.y == triangle1.x && triangle0.z == triangle1.y) || (triangle0.y == triangle1.y && triangle0.z == triangle1.x)){

				adjacentTriangles[k + 3] = triangle1.z;
				adjacentTriangles[t + 1] = triangle0.x;

			}

			// <-- Edge #2 == Edge #2 --> //

			if ((triangle0.y == triangle1.y && triangle0.z == triangle1.z) || (triangle0.y == triangle1.z && triangle0.z == triangle1.y)){

				adjacentTriangles[k + 3] = triangle1.x;
				adjacentTriangles[t + 3] = triangle0.x;

			}

			// <-- Edge #2 == Edge #3 --> //

			if ((triangle0.y == triangle1.z && triangle0.z == triangle1.x) || (triangle0.y == triangle1.x && triangle0.z == triangle1.z)){

				adjacentTriangles[k + 3] = triangle1.y;
				adjacentTriangles[t + 5] = triangle0.x;

			}

			// <-- Edge #3 == Edge #1 --> //

			if ((triangle0.z == triangle1.x && triangle0.x == triangle1.y) || (triangle0.z == triangle1.y && triangle0.x == triangle1.x)){

				adjacentTriangles[k + 5] = triangle1.z;
				adjacentTriangles[t + 1] = triangle0.y;

			}

			// <-- Edge #3 == Edge #2 --> //

			if ((triangle0.z == triangle1.y && triangle0.x == triangle1.z) || (triangle0.z == triangle1.z && triangle0.x == triangle1.y)){

				adjacentTriangles[k + 5] = triangle1.x;
				adjacentTriangles[t + 3] = triangle0.y;

			}

			// <-- Edge #3 == Edge #3 --> //

			if ((triangle0.z == triangle1.z && triangle0.x == triangle1.x) || (triangle0.z == triangle1.x && triangle0.x == triangle1.z)){

				adjacentTriangles[k + 5] = triangle1.y;
				adjacentTriangles[t + 5] = triangle0.y;

			}

		}

	}

	for (int k = 0; k < static_cast<GLint>(adjacentTriangles.size()); k += 6){

		if (adjacentTriangles[k + 1] == -1)
			adjacentTriangles[k + 1] = adjacentTriangles[k + 4];

		if (adjacentTriangles[k + 3] == -1)
			adjacentTriangles[k + 3] = adjacentTriangles[k + 0];

		if (adjacentTriangles[k + 5] == -1)
			adjacentTriangles[k + 5] = adjacentTriangles[k + 2];

	}

	mIndexes = adjacentTriangles;

}

// <-- Computes bounding box lines --> //

void CLoader::computeBoundingBox(){

	// <-- First line --> //

	mBoundingBox.push_back(mBoundingBoxMin.x);
	mBoundingBox.push_back(mBoundingBoxMin.y);
	mBoundingBox.push_back(mBoundingBoxMin.z);
	mBoundingBox.push_back(mBoundingBoxMin.x);
	mBoundingBox.push_back(mBoundingBoxMin.y);
	mBoundingBox.push_back(mBoundingBoxMax.z);

	// <-- Second line --> //

	mBoundingBox.push_back(mBoundingBoxMin.x);
	mBoundingBox.push_back(mBoundingBoxMax.y);
	mBoundingBox.push_back(mBoundingBoxMin.z);
	mBoundingBox.push_back(mBoundingBoxMin.x);
	mBoundingBox.push_back(mBoundingBoxMax.y);
	mBoundingBox.push_back(mBoundingBoxMax.z);

	// <-- Third line --> //

	mBoundingBox.push_back(mBoundingBoxMax.x);
	mBoundingBox.push_back(mBoundingBoxMin.y);
	mBoundingBox.push_back(mBoundingBoxMin.z);
	mBoundingBox.push_back(mBoundingBoxMax.x);
	mBoundingBox.push_back(mBoundingBoxMin.y);
	mBoundingBox.push_back(mBoundingBoxMax.z);

	// <-- Fourth line --> //

	mBoundingBox.push_back(mBoundingBoxMax.x);
	mBoundingBox.push_back(mBoundingBoxMax.y);
	mBoundingBox.push_back(mBoundingBoxMin.z);
	mBoundingBox.push_back(mBoundingBoxMax.x);
	mBoundingBox.push_back(mBoundingBoxMax.y);
	mBoundingBox.push_back(mBoundingBoxMax.z);

	// <-- Fifth line --> //

	mBoundingBox.push_back(mBoundingBoxMin.x);
	mBoundingBox.push_back(mBoundingBoxMin.y);
	mBoundingBox.push_back(mBoundingBoxMin.z);
	mBoundingBox.push_back(mBoundingBoxMin.x);
	mBoundingBox.push_back(mBoundingBoxMax.y);
	mBoundingBox.push_back(mBoundingBoxMin.z);

	// <-- Sixth line --> //

	mBoundingBox.push_back(mBoundingBoxMin.x);
	mBoundingBox.push_back(mBoundingBoxMin.y);
	mBoundingBox.push_back(mBoundingBoxMax.z);
	mBoundingBox.push_back(mBoundingBoxMin.x);
	mBoundingBox.push_back(mBoundingBoxMax.y);
	mBoundingBox.push_back(mBoundingBoxMax.z);

	// <-- Seventh line --> //

	mBoundingBox.push_back(mBoundingBoxMax.x);
	mBoundingBox.push_back(mBoundingBoxMin.y);
	mBoundingBox.push_back(mBoundingBoxMin.z);
	mBoundingBox.push_back(mBoundingBoxMax.x);
	mBoundingBox.push_back(mBoundingBoxMax.y);
	mBoundingBox.push_back(mBoundingBoxMin.z);

	// <-- Eighth line --> //

	mBoundingBox.push_back(mBoundingBoxMax.x);
	mBoundingBox.push_back(mBoundingBoxMin.y);
	mBoundingBox.push_back(mBoundingBoxMax.z);
	mBoundingBox.push_back(mBoundingBoxMax.x);
	mBoundingBox.push_back(mBoundingBoxMax.y);
	mBoundingBox.push_back(mBoundingBoxMax.z);

	// <-- Ninth line --> //

	mBoundingBox.push_back(mBoundingBoxMin.x);
	mBoundingBox.push_back(mBoundingBoxMin.y);
	mBoundingBox.push_back(mBoundingBoxMin.z);
	mBoundingBox.push_back(mBoundingBoxMax.x);
	mBoundingBox.push_back(mBoundingBoxMin.y);
	mBoundingBox.push_back(mBoundingBoxMin.z);

	// <-- Tenth line --> //

	mBoundingBox.push_back(mBoundingBoxMin.x);
	mBoundingBox.push_back(mBoundingBoxMax.y);
	mBoundingBox.push_back(mBoundingBoxMin.z);
	mBoundingBox.push_back(mBoundingBoxMax.x);
	mBoundingBox.push_back(mBoundingBoxMax.y);
	mBoundingBox.push_back(mBoundingBoxMin.z);

	// <-- Eleventh line --> //

	mBoundingBox.push_back(mBoundingBoxMin.x);
	mBoundingBox.push_back(mBoundingBoxMin.y);
	mBoundingBox.push_back(mBoundingBoxMax.z);
	mBoundingBox.push_back(mBoundingBoxMax.x);
	mBoundingBox.push_back(mBoundingBoxMin.y);
	mBoundingBox.push_back(mBoundingBoxMax.z);

	// <-- Twelfth line --> //

	mBoundingBox.push_back(mBoundingBoxMin.x);
	mBoundingBox.push_back(mBoundingBoxMax.y);
	mBoundingBox.push_back(mBoundingBoxMax.z);
	mBoundingBox.push_back(mBoundingBoxMax.x);
	mBoundingBox.push_back(mBoundingBoxMax.y);
	mBoundingBox.push_back(mBoundingBoxMax.z);

	// <-- Compute bounding box mid point --> //

	mBoundingBoxMid.x = (mBoundingBoxMin.x + mBoundingBoxMax.x) / 2.0f;
	mBoundingBoxMid.y = (mBoundingBoxMin.y + mBoundingBoxMax.y) / 2.0f;
	mBoundingBoxMid.z = (mBoundingBoxMin.z + mBoundingBoxMax.z) / 2.0f;

}

// <-- Gets bounding box max --> //

vec3 CLoader::getBoundingBoxMax(){

	return mBoundingBoxMax;

}

// <-- Gets bounding box mid --> //

vec3 CLoader::getBoundingBoxMid(){

	return mBoundingBoxMid;

}

// <-- Gets bounding box min --> //

vec3 CLoader::getBoundingBoxMin(){

	return mBoundingBoxMin;

}

// <-- Gets indexes --> //

vector<GLuint> CLoader::getIndexes(){

	return mIndexes;

}

// <-- Gets normals --> //

vector<GLfloat> CLoader::getNormals(){

	return mNormals;

}

// <-- Gets tangents --> //

vector<GLfloat> CLoader::getTangents(){

	return mTangents;

}

// <-- Gets vertexes --> //

vector<GLfloat> CLoader::getVertexes(){

	return mVertexes;

}

// <-- Gets binormals --> //

vector<GLfloat> CLoader::getBinormals(){

	return mBinormals;

}

// <-- Gets bounding box --> //

vector<GLfloat> CLoader::getBoundingBox(){

	return mBoundingBox;

}

// <-- Gets texture coordinates --> //

vector<GLfloat> CLoader::getTextureCoordinates(){

	return mTextureCoordinates;

}

// <-- Get tokens --> //

GLchar ** CLoader::getTokens(GLchar *nLine, const GLchar * nDelimiters, GLuint &nBufferSize){

	GLchar * context, *token, *tLine, ** tokenBuffer;

	token = NULL;
	tokenBuffer = NULL;
	tLine = (GLchar *)malloc(((GLuint)strlen(nLine) + 1) * sizeof(GLchar));

	for (int k = 0; k < (int)strlen(nLine); ++k)
		if (nLine[k] == '\t') nLine[k] = ' ';

	nBufferSize = 0;
	strcpy_s(tLine, (strlen(nLine) + 1) * sizeof(GLchar), nLine);
	token = strtok_s(tLine, nDelimiters, &context);

	while (token){

		tokenBuffer = (GLchar **)realloc(tokenBuffer, ++nBufferSize * sizeof(GLchar*));
		tokenBuffer[nBufferSize - 1] = token;
		token = strtok_s(NULL, nDelimiters, &context);

	}

	return tokenBuffer;

}
