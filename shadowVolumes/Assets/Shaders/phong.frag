#version 430 core

struct meshInfo{
	vec3 mKa;
	vec3 mKd;
	vec3 mKs;
	float mShininess;
};

struct lightInfo{
	vec3 mLa;
	vec3 mLd;
	vec3 mLs;
	int mType;
	float mCutoff;
	vec3 mPosition;
	vec3 mDirection;
	float mExponent;
};

layout(location = 0) out vec4 ambientColor;
layout(location = 1) out vec4 diffuseSpecularColor;
layout(binding = 1) uniform sampler2D diffuseTexture;

uniform meshInfo mesh;
uniform vec3 cameraPosition;
uniform lightInfo lights[1];

in vec3 normalWs;
in vec3 vertexWs;
in vec2 textureCoordinatesWs;

void phongPoint(vec3 V, vec3 N, int k){
	
	vec4 textureColor;
	vec3 ambient, diffuse, specular, diffSpec;

	vec3 S = normalize(lights[k].mPosition - vertexWs);
	vec3 H = normalize(S + V);

	float SdotN = max(dot(S,N), 0.0);

	ambient  = lights[k].mLa * mesh.mKa;
	diffuse  = lights[k].mLd * mesh.mKd * SdotN;
	specular = lights[k].mLs * mesh.mKs * pow(max(dot(H,N),0.0), 0.5 * mesh.mShininess);

	diffSpec = diffuse + specular;

	ambientColor = vec4(ambient,1.0);
	diffuseSpecularColor = vec4(diffSpec, 1.0);
	textureColor = texture(diffuseTexture, textureCoordinatesWs);

	if(textureColor.r != 0 || textureColor.g != 0 || textureColor.b != 0)
		diffuseSpecularColor *= textureColor;

}

void main(){

	vec3 color = vec3(0);
	
	vec3 V = normalize(cameraPosition - vertexWs);
	vec3 N = (gl_FrontFacing) ? normalWs : -normalWs;

	for(int k = 0; k < 1; ++k)
		phongPoint(V,N,k);

}