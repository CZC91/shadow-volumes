#version 430 core

layout(location = 0) out vec4 fragmentColor;
layout(binding = 0) uniform sampler2D shadowTexture;
 
in vec3 normalWs;
in vec3 vertexWs;

void main(){
	
	vec4 diffSpec = texelFetch(shadowTexture, ivec2(gl_FragCoord), 0);
	fragmentColor = vec4(diffSpec.xyz, 1.0);

}