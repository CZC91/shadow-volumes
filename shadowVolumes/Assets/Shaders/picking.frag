#version 430 core

layout(location = 0) out vec4 fragmentColor;

uniform vec3 pickingColor;

void main(){

	fragmentColor = vec4(pickingColor,1.0);

}