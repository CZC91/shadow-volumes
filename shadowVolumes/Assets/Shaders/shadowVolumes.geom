#version 430 core

layout(triangles_adjacency) in;
layout(triangle_strip, max_vertices = 18) out;

in vec3 vertexWs[];
in vec3 normalWs[];
in vec2 textureCoordinatesWs[];

uniform mat4 viewMatrix;
uniform vec3 lightPosition;
uniform mat4 projectionMatrix;

bool facesLight(vec3 a, vec3 b, vec3 c){

  vec3 n = cross(b - a, c - a);
  vec3 da = lightPosition - a;
  vec3 db = lightPosition - b;
  vec3 dc = lightPosition - c;

  return dot(n, da) > 0 || dot(n, db) > 0 || dot(n, dc) > 0; 

}

void emitEdgeQuad(vec3 a, vec3 b){

  gl_Position = projectionMatrix * viewMatrix * vec4(a, 1);
  EmitVertex();
  
  gl_Position = projectionMatrix * viewMatrix * vec4(a - lightPosition, 0);
  EmitVertex();

  gl_Position = projectionMatrix * viewMatrix * vec4(b, 1);
  EmitVertex();

  gl_Position = projectionMatrix * viewMatrix * vec4(b - lightPosition, 0);
  EmitVertex();

  EndPrimitive();

}

void main(){

	if(facesLight(vertexWs[0], vertexWs[2], vertexWs[4])){

		if(!facesLight(vertexWs[0],vertexWs[1],vertexWs[2])) 
          emitEdgeQuad(vertexWs[0],vertexWs[2]);

        if(!facesLight(vertexWs[2],vertexWs[3],vertexWs[4])) 
          emitEdgeQuad(vertexWs[2],vertexWs[4]);

        if(!facesLight(vertexWs[4],vertexWs[5],vertexWs[0])) 
          emitEdgeQuad(vertexWs[4],vertexWs[0]);
	
	}

}