#version 430 core

layout(location = 0) in vec3 normal;
layout(location = 1) in vec3 vertex;

uniform mat4 modelMatrix;
uniform mat3 normalMatrix;
uniform mat4 modelViewProjectionMatrix;

out vec3 normalWs;
out vec3 vertexWs;

void main(){

	normalWs = normalize(normalMatrix * normal);
	vertexWs = vec3(modelMatrix * vec4(vertex,1.0));
	gl_Position = vec4(vertex.x, vertex.y, 0.0, 1.0);
  
}