#version 430 core

layout(location = 0) out vec4 fragmentColor;

uniform vec3 La;
uniform vec3 Ld;
uniform vec3 Ls;

void main(){

	vec3 color;

	color.x = (La.x + Ld.x + Ls.x) / 3.0;
	color.y = (La.y + Ld.y + Ls.y) / 3.0;
	color.z = (La.z + Ld.z + Ls.z) / 3.0;

	fragmentColor = vec4(color, 1.0);

}