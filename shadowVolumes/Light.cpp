#include "Light.h"

// <-- Constructor --> //

CLight::CLight(){

	mpBuffer = new CBuffer();

}

// <-- Destructor --> //

CLight::~CLight(){

	delete mpBuffer;

}

// <-- Draw light using Buffer class --> //

void CLight::draw(){

	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		mpBuffer->draw();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

}

// <-- Draw bounding box using Buffer class--> //

void CLight::drawBoundingBox(){

	mpBuffer->drawBoundingBox();

}

// <-- Updates light position --> //

void CLight::updatePosition(const GLfloat nAngle){

	mPosition = glm::rotateY(mPosition, glm::degrees(nAngle));

}

// <-- Compare picking color --> //

bool CLight::comparePickingColor(const vec3 nPickingColor){

	return (mPickingColor.x == nPickingColor.x) && (mPickingColor.y == nPickingColor.y) && (mPickingColor.z == nPickingColor.z);

}

// <-- Gets ambiental coefficent --> //

vec3 CLight::getLa(){

	return mLa;

}

// <-- Gets diffuse coefficent --> //

vec3 CLight::getLd(){

	return mLd;

}

// <-- Gets specular coefficent --> //

vec3 CLight::getLs(){

	return mLs;

}

// <-- Gets light type --> //

GLint CLight::getType(){

	return mType;

}

// <-- Gets position --> //

vec3 CLight::getPosition(){

	return mPosition;

}

// <-- Gets direction --> //

vec3 CLight::getDirection(){

	return mDirection;

}

// <-- Gets Cutoff --> //

GLfloat CLight::getCutoff(){

	return mCutoff;

}

// <-- Gets exponent --> //

GLfloat CLight::getExponent(){

	return mExponent;

}

// <-- Gets model matrix --> //

mat4 CLight::getModelMatrix(){

	mModelMatrix = mat4(1.0f);
	mModelMatrix = translate(mModelMatrix, mPosition);

	return mModelMatrix;

}

// <-- Gets picking color --> //

vec3 CLight::getPickingColor(){

	return mPickingColor;

}

// <-- Sets ambiental coefficent --> //

void CLight::setLa(const vec3 nLa){

	mLa = nLa;

}

// <-- Sets diffuse coefficent --> //

void CLight::setLd(const vec3 nLd){

	mLd = nLd;

}

// <-- Sets specular coefficent --> //

void CLight::setLs(const vec3 nLs){

	mLs = nLs;

}

// <-- Sets light type --> //

void CLight::setType(const GLint nType){

	mType = nType;

}

// <-- Sets cutoff --> //

void CLight::setCutoff(const GLfloat nCutoff){

	mCutoff = nCutoff;

}

// <-- Sets position --> //

void CLight::setPosition(const vec3 nPosition){

	mPosition = nPosition;

}

// <-- Sets direction --> //

void CLight::setDirection(const vec3 nDirection){

	mDirection = nDirection;

}

// <-- Sets exponent --> //

void CLight::setExponent(const GLfloat nExponent){

	mExponent = nExponent;

}

// <-- Sets picking color --> //

void CLight::setPickingColor(const vec3 nPickingColor){

	mPickingColor = nPickingColor;

}
