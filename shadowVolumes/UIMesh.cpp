#include "UIMesh.h"

// <-- Constructor --> //

CUIMesh::CUIMesh(){

}

// <-- Destructor --> //

CUIMesh::~CUIMesh(){

	TwDeleteBar(mpBar);

}

// <-- Draws mesh user interface --> //

void CUIMesh::draw(){

	TwDefine("Mesh visible = true");

}

// <-- Hides mesh user interface --> //

void CUIMesh::hide(){

	TwDefine("Mesh visible = false");

}

// <-- Inits mesh user interface --> //

void CUIMesh::init(){

	// <-- Creating a new Bar --> //

	mpBar = TwNewBar("Mesh");

	// <-- Bar parameters --> //

	TwDefine("Mesh movable=true");
	TwDefine("Mesh visible=false");
	TwDefine("Mesh resizable=false");
	TwDefine("Mesh color='100 100 100'");
	TwDefine("Mesh position='20 20'");
	TwDefine("Mesh fontresizable=false");

	// <-- Creating AntTweakBar rotation quaternion --> //

	TwAddVarRW(mpBar, "mRotation", TW_TYPE_QUAT4F, glm::value_ptr(mRotation), "label='Rotation'");
	TwAddSeparator(mpBar, "mSeparator0", "");

	// <-- Creating AntTweakBar float that works as shininess --> //

	TwAddVarRW(mpBar, "mShininess", TW_TYPE_FLOAT, &mShininess, "label='Shininess' min = 0.0 max = 200.0 step = 1.0");
	TwAddSeparator(mpBar, "mSeparator1", "");

	// <-- Creating three AntTweakBar floats that work as position in 3D space --> //

	TwAddVarRW(mpBar, "mPositionX", TW_TYPE_FLOAT, &mPosition.x, "label='Position X' step = 0.01");
	TwAddVarRW(mpBar, "mPositionY", TW_TYPE_FLOAT, &mPosition.y, "label='Position Y' step = 0.01");
	TwAddVarRW(mpBar, "mPositionZ", TW_TYPE_FLOAT, &mPosition.z, "label='Position Z' step = 0.01");
	TwAddSeparator(mpBar, "mSeparator2", "");

	// <-- Creating three AntTweakBar floats that work as scaling in 3D space --> //

	TwAddVarRW(mpBar, "mScalingX", TW_TYPE_FLOAT, &mScaling.x, "label='Scaling X' step = 0.01");
	TwAddVarRW(mpBar, "mScalingY", TW_TYPE_FLOAT, &mScaling.y, "label='Scaling Y' step = 0.01");
	TwAddVarRW(mpBar, "mScalingZ", TW_TYPE_FLOAT, &mScaling.z, "label='Scaling Z' step = 0.01");
	TwAddSeparator(mpBar, "mSeparator3", "");

	// <-- Creating three AntTweakBar colors that work as ambiental, diffuse and specular coefficents --> //

	TwAddVarRW(mpBar, "mKa", TW_TYPE_COLOR3F, glm::value_ptr(mKa), "label='Ka'");
	TwAddVarRW(mpBar, "mKd", TW_TYPE_COLOR3F, glm::value_ptr(mKd), "label='Kd'");
	TwAddVarRW(mpBar, "mKs", TW_TYPE_COLOR3F, glm::value_ptr(mKs), "label='Ks'");
	TwAddSeparator(mpBar, "mSeparator4", "");

}

// <-- Gets ambiental coefficent --> //

vec3 CUIMesh::getKa(){

	return mKa;

}

// <-- Gets diffuse coefficent --> //

vec3 CUIMesh::getKd(){

	return mKd;

}

// <-- Gets specular coefficent --> //

vec3 CUIMesh::getKs(){

	return mKs;

}

// <-- Gets scale (x,y,z) --> //

vec3 CUIMesh::getScaling(){

	return mScaling;

}

// <-- Gets position (x,y,z) --> //

vec3 CUIMesh::getPosition(){

	return mPosition;

}

// <-- Gets rotation (x,y,z) --> //

quat CUIMesh::getRotation(){

	return mRotation;

}

// <-- Gets shininess --> //

GLfloat CUIMesh::getShininess(){

	return mShininess;

}

// <-- Sets ambiental coefficent --> //

void CUIMesh::setKa(const vec3 nKa){

	mKa = nKa;

}

// <-- Sets diffuse coefficent --> //

void CUIMesh::setKd(const vec3 nKd){

	mKd = nKd;

}

// <-- Sets specular coefficent --> //

void CUIMesh::setKs(const vec3 nKs){

	mKs = nKs;

}

// <-- Sets scale (x,y,z) --> //

void CUIMesh::setScaling(const vec3 nScaling){

	mScaling = nScaling;

}

// <-- Sets position (x,y,z) --> //

void CUIMesh::setPosition(const vec3 nPosition){

	mPosition = nPosition;

}

// <-- Sets rotation (x,y,z) --> //

void CUIMesh::setRotation(const quat nRotation){

	mRotation = nRotation;

}

// <-- Sets shininess --> //

void CUIMesh::setShininess(const GLfloat nShininess){

	mShininess = nShininess;

}
