#include "Mesh.h"

// <-- Constructor --> //

CMesh::CMesh(){

	mpBuffer = new CBuffer();

}

// <-- Destructor --> //

CMesh::~CMesh(){

	delete mpBuffer;

}

// <-- Draw mesh using Buffer class --> //

void CMesh::draw(){

	mpBuffer->draw();

}

// <-- Draw bounding box using Buffer class --> //

void CMesh::drawBoundingBox(){

	mpBuffer->drawBoundingBox();

}

// <-- Compare picking color --> //

bool CMesh::comparePickingColor(const vec3 nPickingColor){

	return (mPickingColor.x == nPickingColor.x) && (mPickingColor.y == nPickingColor.y) && (mPickingColor.z == nPickingColor.z);

}

// <-- Returns ambiental coefficent --> //

vec3 CMesh::getKa(){

	return mKa;

}

// <-- Returns diffuse coefficent --> //

vec3 CMesh::getKd(){

	return mKd;

}

// <-- Returns specular coefficent --> //

vec3 CMesh::getKs(){

	return mKs;

}

// <-- Returns scaling (x,y,z) --> //

vec3 CMesh::getScaling(){

	return mScaling;

}

// <-- Returns mid point --> //

vec3 CMesh::getMidPoint(){

	return mMidPoint;

}

// <-- Returns position (x,y,z) --> //

vec3 CMesh::getPosition(){

	return mPosition;

}

// <-- Returns rotation (x,y,z) as quaternion --> //

quat CMesh::getRotation(){

	return mRotation;

}

// <-- Gets texture ID --> //

GLuint CMesh::getTextureID(){

	return mTextureID;

}

// <-- Returns model matrix --> //

mat4 CMesh::getModelMatrix(){

	mModelMatrix = mat4(1.0);
	mModelMatrix = translate(mModelMatrix, mPosition);
	mModelMatrix = translate(mModelMatrix, mMidPoint);
	mModelMatrix = mModelMatrix * toMat4(mRotation);
	mModelMatrix = scale(mModelMatrix, mScaling);
	mModelMatrix = translate(mModelMatrix, -mMidPoint);

	return mModelMatrix;

}

// <-- Returns picking color --> //

vec3 CMesh::getPickingColor(){

	return mPickingColor;

}

// <-- Returns shininess factor --> //

GLfloat CMesh::getShininess(){

	return mShininess;

}

// <-- Gets shadow caster --> //

bool CMesh::getShadowCaster(){

	return mShadowCaster;

}

// <-- Sets ambiental coefficent --> //

void CMesh::setKa(const vec3 nKa){

	mKa = nKa;

}

// <-- Sets diffuse coefficent --> //

void CMesh::setKd(const vec3 nKd){

	mKd = nKd;

}

// <-- Sets specular coefficent --> //

void CMesh::setKs(const vec3 nKs){

	mKs = nKs;

}

// <-- Sets scaling (x,y,z) --> //

void CMesh::setScaling(const vec3 nScaling){

	mScaling = nScaling;

}

// <-- Sets mid point --> //

void CMesh::setMidPoint(const vec3 nMidPoint){

	mMidPoint = nMidPoint;

}

// <-- Sets position (x,y,z) --> //

void CMesh::setPosition(const vec3 nPosition){

	mPosition = nPosition;

}

// <-- Sets rotation (x,y,z) --> //

void CMesh::setRotation(const quat nRotation){

	mRotation = nRotation;

}

// <-- Sets texture ID --> //

void CMesh::setTextureID(const GLuint nTextureID){

	mTextureID = nTextureID;

}

// <-- Sets shininess factor --> //

void CMesh::setShininess(const GLfloat nShininess){

	mShininess = nShininess;

}

// <-- Sets picking color --> //

void CMesh::setPickingColor(const vec3 nPickingColor){

	mPickingColor = nPickingColor;

}

// <-- Sets shadow caster --> //

void CMesh::setShadowCaster(const bool nShadowCaster){

	mShadowCaster = nShadowCaster;

}
