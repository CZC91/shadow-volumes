#ifndef TEXTURE_LOADER
#define TEXTURE_LOADER

#include <Core\gl_core_4_3.h>
#include <IL\il.h>
#include <IL\ilu.h>
#include <IL\ilut.h>

class CTextureLoader{

private:

public:

	CTextureLoader();
	~CTextureLoader();
	
	GLuint load(const char *npFilePath);

};

#endif