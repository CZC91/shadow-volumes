# Shadow Volumes Application - README


##Assets


Assets folder on dropbox link : [Here](https://www.dropbox.com/s/701dcb7fp41muni/ShadowVolumesAssets.rar?dl=0)


##VS 2013 Project Configuration

###Configuration of additional libraries


+ Go to : Project Properties -> C/C++ -> General -> Additional Include Directories
+ Paste : $(ProjectDir)\Assets\Libraries;


###Configuration of project linker


+ Go to : Project Properties -> Linker -> General -> Additional Library Directories
+ Paste : $(ProjectDir)\Assets\Libs;


+ Go to : Project Properties -> Linker -> Input -> Additional Dependencies
+ Paste : OpenGL32.lib glfw3dll.lib AntTweakBar.lib ILU.lib ILUT.lib DevIL.lib Assimp.lib


Carlos Zapata Conforto